#TP 3 Quentin DUMAS

##Setup

###Installation de docker


#### Set up du dossier

```
[bagarre@docker ~]$ sudo dnf install yum-utils
Last metadata expiration check: 19:40:54 ago on Wed 08 Dec 2021 04:11:16 PM CET.
Dependencies resolved.
===============================================================================================================================
 Package                       Architecture               Version                             Repository                  Size
===============================================================================================================================
Installing:
 yum-utils                     noarch                     4.0.21-3.el8                        baseos                      71 k

Transaction Summary
===============================================================================================================================
Install  1 Package

Total download size: 71 k
Installed size: 22 k
Is this ok [y/N]: y
Downloading Packages:
yum-utils-4.0.21-3.el8.noarch.rpm                                                               68 kB/s |  71 kB     00:01
-------------------------------------------------------------------------------------------------------------------------------
Total                                                                                           54 kB/s |  71 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                       1/1
  Installing       : yum-utils-4.0.21-3.el8.noarch                                                                         1/1
  Running scriptlet: yum-utils-4.0.21-3.el8.noarch                                                                         1/1
  Verifying        : yum-utils-4.0.21-3.el8.noarch                                                                         1/1

Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!
[bagarre@docker ~]$ sudo yum-config-manager \
> --add-repo \
> https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[bagarre@docker ~]$ sudo yum-config-manager --enable docker-ce-nightly
[bagarre@docker ~]$ sudo yum-config-manager --enable docker-ce-test
```

#### Docker Engine

```

[bagarre@docker ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Docker CE Stable - x86_64                                                                      115 kB/s |  19 kB     00:00
Docker CE Test - x86_64                                                                        163 kB/s |  22 kB     00:00
Docker CE Nightly - x86_64                                                                      25 kB/s | 4.6 kB     00:00
Dependencies resolved.
===============================================================================================================================
 Package                            Architecture Version                                          Repository              Size
===============================================================================================================================
Installing:
 containerd.io                      x86_64       1.4.12-3.1.el8                                   docker-ce-stable        28 M
 docker-ce                          x86_64       3:20.10.11-3.el8                                 docker-ce-stable        22 M
 docker-ce-cli                      x86_64       1:20.10.11-3.el8                                 docker-ce-stable        29 M
Installing dependencies:
 checkpolicy                        x86_64       2.9-1.el8                                        baseos                 345 k
 container-selinux                  noarch       2:2.167.0-1.module+el8.5.0+710+4c471e88          appstream               53 k
 docker-ce-rootless-extras          x86_64       20.10.11-3.el8                                   docker-ce-stable       4.6 M
 docker-scan-plugin                 x86_64       0.9.0-3.el8                                      docker-ce-stable       3.7 M
 fuse-common                        x86_64       3.2.1-12.el8                                     baseos                  20 k
 fuse-overlayfs                     x86_64       1.7.1-1.module+el8.5.0+710+4c471e88              appstream               71 k
 fuse3                              x86_64       3.2.1-12.el8                                     baseos                  49 k
 fuse3-libs                         x86_64       3.2.1-12.el8                                     baseos                  93 k
 libcgroup                          x86_64       0.41-19.el8                                      baseos                  69 k
 libslirp                           x86_64       4.4.0-1.module+el8.5.0+710+4c471e88              appstream               69 k
 policycoreutils-python-utils       noarch       2.9-16.el8                                       baseos                 251 k
 python3-audit                      x86_64       3.0-0.17.20191104git1c2f876.el8.1                baseos                  85 k
 python3-libsemanage                x86_64       2.9-6.el8                                        baseos                 126 k
 python3-policycoreutils            noarch       2.9-16.el8                                       baseos                 2.2 M
 python3-setools                    x86_64       4.3.0-2.el8                                      baseos                 625 k
 slirp4netns                        x86_64       1.1.8-1.module+el8.5.0+710+4c471e88              appstream               50 k
Enabling module streams:
 container-tools                                 rhel8

Transaction Summary
===============================================================================================================================
Install  19 Packages

Total download size: 92 M
Installed size: 382 M
Is this ok [y/N]: y
Downloading Packages:
(1/19): container-selinux-2.167.0-1.module+el8.5.0+710+4c471e88.noarch.rpm                     286 kB/s |  53 kB     00:00
(2/19): fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64.rpm                          386 kB/s |  71 kB     00:00
(3/19): libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                371 kB/s |  69 kB     00:00
(4/19): fuse-common-3.2.1-12.el8.x86_64.rpm                                                    397 kB/s |  20 kB     00:00
(5/19): slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64.rpm                             679 kB/s |  50 kB     00:00
(6/19): fuse3-3.2.1-12.el8.x86_64.rpm                                                          1.1 MB/s |  49 kB     00:00
(7/19): fuse3-libs-3.2.1-12.el8.x86_64.rpm                                                     1.7 MB/s |  93 kB     00:00
(8/19): checkpolicy-2.9-1.el8.x86_64.rpm                                                       2.6 MB/s | 345 kB     00:00
(9/19): libcgroup-0.41-19.el8.x86_64.rpm                                                       1.7 MB/s |  69 kB     00:00
(10/19): python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64.rpm                            2.3 MB/s |  85 kB     00:00
(11/19): python3-libsemanage-2.9-6.el8.x86_64.rpm                                              2.4 MB/s | 126 kB     00:00
(12/19): policycoreutils-python-utils-2.9-16.el8.noarch.rpm                                    2.2 MB/s | 251 kB     00:00
(13/19): python3-setools-4.3.0-2.el8.x86_64.rpm                                                2.3 MB/s | 625 kB     00:00
(14/19): python3-policycoreutils-2.9-16.el8.noarch.rpm                                         1.9 MB/s | 2.2 MB     00:01
(15/19): containerd.io-1.4.12-3.1.el8.x86_64.rpm                                               4.4 MB/s |  28 MB     00:06
(16/19): docker-ce-rootless-extras-20.10.11-3.el8.x86_64.rpm                                   4.9 MB/s | 4.6 MB     00:00
(17/19): docker-scan-plugin-0.9.0-3.el8.x86_64.rpm                                             4.7 MB/s | 3.7 MB     00:00
(18/19): docker-ce-cli-20.10.11-3.el8.x86_64.rpm                                               2.6 MB/s |  29 MB     00:11
(19/19): docker-ce-20.10.11-3.el8.x86_64.rpm                                                   1.7 MB/s |  22 MB     00:13
-------------------------------------------------------------------------------------------------------------------------------
Total                                                                                          6.5 MB/s |  92 MB     00:14
Docker CE Stable - x86_64                                                                       17 kB/s | 1.6 kB     00:00
Importing GPG key 0x621E9F35:
 Userid     : "Docker Release (CE rpm) <docker@docker.com>"
 Fingerprint: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
 From       : https://download.docker.com/linux/centos/gpg
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                       1/1
  Installing       : docker-scan-plugin-0.9.0-3.el8.x86_64                                                                1/19
  Running scriptlet: docker-scan-plugin-0.9.0-3.el8.x86_64                                                                1/19
  Installing       : docker-ce-cli-1:20.10.11-3.el8.x86_64                                                                2/19
  Running scriptlet: docker-ce-cli-1:20.10.11-3.el8.x86_64                                                                2/19
  Installing       : python3-setools-4.3.0-2.el8.x86_64                                                                   3/19
  Installing       : python3-libsemanage-2.9-6.el8.x86_64                                                                 4/19
  Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                               5/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                         6/19
  Installing       : libcgroup-0.41-19.el8.x86_64                                                                         6/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                         6/19
  Installing       : fuse3-libs-3.2.1-12.el8.x86_64                                                                       7/19
  Running scriptlet: fuse3-libs-3.2.1-12.el8.x86_64                                                                       7/19
  Installing       : fuse-common-3.2.1-12.el8.x86_64                                                                      8/19
  Installing       : fuse3-3.2.1-12.el8.x86_64                                                                            9/19
  Installing       : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                           10/19
  Running scriptlet: fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                           10/19
  Installing       : checkpolicy-2.9-1.el8.x86_64                                                                        11/19
  Installing       : python3-policycoreutils-2.9-16.el8.noarch                                                           12/19
  Installing       : policycoreutils-python-utils-2.9-16.el8.noarch                                                      13/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                    14/19
  Installing       : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                    14/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                    14/19
  Installing       : containerd.io-1.4.12-3.1.el8.x86_64                                                                 15/19
  Running scriptlet: containerd.io-1.4.12-3.1.el8.x86_64                                                                 15/19
  Installing       : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                 16/19
  Installing       : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                              17/19
  Installing       : docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                     18/19
  Running scriptlet: docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                     18/19
  Installing       : docker-ce-3:20.10.11-3.el8.x86_64                                                                   19/19
  Running scriptlet: docker-ce-3:20.10.11-3.el8.x86_64                                                                   19/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                    19/19
  Running scriptlet: docker-ce-3:20.10.11-3.el8.x86_64                                                                   19/19
  Verifying        : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                     1/19
  Verifying        : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                            2/19
  Verifying        : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                  3/19
  Verifying        : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                               4/19
  Verifying        : checkpolicy-2.9-1.el8.x86_64                                                                         5/19
  Verifying        : fuse-common-3.2.1-12.el8.x86_64                                                                      6/19
  Verifying        : fuse3-3.2.1-12.el8.x86_64                                                                            7/19
  Verifying        : fuse3-libs-3.2.1-12.el8.x86_64                                                                       8/19
  Verifying        : libcgroup-0.41-19.el8.x86_64                                                                         9/19
  Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch                                                      10/19
  Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                              11/19
  Verifying        : python3-libsemanage-2.9-6.el8.x86_64                                                                12/19
  Verifying        : python3-policycoreutils-2.9-16.el8.noarch                                                           13/19
  Verifying        : python3-setools-4.3.0-2.el8.x86_64                                                                  14/19
  Verifying        : containerd.io-1.4.12-3.1.el8.x86_64                                                                 15/19
  Verifying        : docker-ce-3:20.10.11-3.el8.x86_64                                                                   16/19
  Verifying        : docker-ce-cli-1:20.10.11-3.el8.x86_64                                                               17/19
  Verifying        : docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                     18/19
  Verifying        : docker-scan-plugin-0.9.0-3.el8.x86_64                                                               19/19

Installed:
  checkpolicy-2.9-1.el8.x86_64                               container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch
  containerd.io-1.4.12-3.1.el8.x86_64                        docker-ce-3:20.10.11-3.el8.x86_64
  docker-ce-cli-1:20.10.11-3.el8.x86_64                      docker-ce-rootless-extras-20.10.11-3.el8.x86_64
  docker-scan-plugin-0.9.0-3.el8.x86_64                      fuse-common-3.2.1-12.el8.x86_64
  fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64  fuse3-3.2.1-12.el8.x86_64
  fuse3-libs-3.2.1-12.el8.x86_64                             libcgroup-0.41-19.el8.x86_64
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64        policycoreutils-python-utils-2.9-16.el8.noarch
  python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64     python3-libsemanage-2.9-6.el8.x86_64
  python3-policycoreutils-2.9-16.el8.noarch                  python3-setools-4.3.0-2.el8.x86_64
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64

Complete!

[bagarre@docker ~]$ sudo systemctl start docker
[bagarre@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[bagarre@docker ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-12-09 11:59:04 CET; 10s ago
     Docs: https://docs.docker.com
 Main PID: 4528 (dockerd)
    Tasks: 7
   Memory: 37.9M
   CGroup: /system.slice/docker.service
           └─4528 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.091995615+01:00" level=warning msg="Your kernel does >
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.092146595+01:00" level=warning msg="Your kernel does >
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.092241134+01:00" level=info msg="Loading containers: >
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.499727368+01:00" level=info msg="Default bridge (dock>
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.574498829+01:00" level=info msg="Firewalld: interface>
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.680723029+01:00" level=info msg="Loading containers: >
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.728431149+01:00" level=info msg="Docker daemon" commi>
Dec 09 11:59:04 docker.tp3.cesi dockerd[4528]: time="2021-12-09T11:59:04.728507271+01:00" level=info msg="Daemon has completed>
Dec 09 11:59:04 docker.tp3.cesi systemd[1]: Started Docker Application Container Engine.
Dec 09 11:59:04 docker.tp3.

```

#### Setup Dcoker 
```

[bagarre@docker ~]$ sudo usermod -aG docker bagarre
[sudo] password for bagarre:
[bagarre@docker ~]$ groups bagarre
bagarre : wheel docker

[bagarre@docker ~]$ sudo systemctl enable docker
[sudo] password for bagarre:
[bagarre@docker ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.11
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 7b11cfaabd73bb80907dd23182b9347b4245eb5d
 runc version: v1.0.2-0-g52b36a2
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 4.18.0-348.el8.0.2.x86_64
 Operating System: Rocky Linux 8.5 (Green Obsidian)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 809.2MiB
 Name: docker.tp3.cesi
 ID: 7RVL:5QJY:A4IS:YEKD:M7FS:QRBW:UVOS:4QR4:6WXP:RD46:E63M:LSMS
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false

```

## II Premier pas 
```
[bagarre@docker ~]$ docker run -t -d -p 8080:80 -v /etc/dockertest/index.html:/var/www/html/cesi/index.html -v /etc/dockertest/nginx.conf:/etc/nginx/nginx.conf --name nginxtrucmachin nginx:latest
[bagarre@docker ~]$ curl 10.2.1.14:8080
<html>
        <body>
                WSH LA STREEET
        </body>
</html>

```
#### Recupérer un terminal:
```
docker container exec -it nginxtrucmachin bash
```

#### L'ip du docker :
```
[bagarre@docker ~]$ docker inspect nginxtrucmachin
...
"Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "cc3be04882ee071b693f3e3531579eac8f71d31d50441ac5fc5fc103eb183f89",
                    "EndpointID": "b50bf3e5b0ba42b99fbe8b1a84fed2612772c5cf83f46713dc3e5ef367c28a4e",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
...
```

#### Limité la RAM
```
[bagarre@docker ~]$ docker run -m 128MB -d --name trucmuche nginx:latest
d7d9dc00c5df577e4d9a02118010255740bf7d46db389489fb479ecd729592e1
[bagarre@docker ~]$ docker ps -a
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                                   NAMES
d7d9dc00c5df   nginx:latest   "/docker-entrypoint.…"   3 seconds ago    Up 1 second     80/tcp                                  trucmuche
e832c545685c   nginx:latest   "/docker-entrypoint.…"   15 minutes ago   Up 15 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nginxtrucmachin
[bagarre@docker ~]$ docker stats trucmuche
CONTAINER ID   NAME        CPU %     MEM USAGE / LIMIT   MEM %     NET I/O     BLOCK I/O         PIDS
d7d9dc00c5df   trucmuche   0.00%     4.223MiB / 128MiB   3.30%     796B / 0B   5.92MB / 22.5kB   2

```

### 2. Vraie application

#### Réseau docker

```

[bagarre@docker ~]$ docker network create wiki
79bebd19aa74f91762cf03691c42115fff482f468cbae0b71f75fc41ec202202
[bagarre@docker ~]$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
cc3be04882ee   bridge    bridge    local
c7002fefd0ed   host      host      local
833698252a9a   none      null      local
79bebd19aa74   wiki      bridge    local
[bagarre@docker ~]$ docker network inspect wiki
[
    {
        "Name": "wiki",
        "Id": "79bebd19aa74f91762cf03691c42115fff482f468cbae0b71f75fc41ec202202",
        "Created": "2021-12-09T15:27:37.229971282+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

```

#### Lancez un conteneur MySQL
