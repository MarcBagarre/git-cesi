# TP 2 Quentin Dumas

# MariaDB

## Installation 🌞
```
[bagarre@db ~]$ sudo dnf install mariadb-server
Last metadata expiration check: 1 day, 0:29:13 ago on Mon 06 Dec 2021 02:27:17 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                              Architecture     Version                                              Repository           Size
======================================================================================================================================
Installing:
 mariadb-server                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream            16 M
Installing dependencies:
 mariadb                              x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream           6.0 M
 mariadb-common                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream            62 k
 mariadb-connector-c                  x86_64           3.1.11-2.el8_3                                       appstream           199 k
 mariadb-connector-c-config           noarch           3.1.11-2.el8_3                                       appstream            14 k
 mariadb-errmsg                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream           233 k
 perl-DBD-MySQL                       x86_64           4.046-3.module+el8.4.0+577+b8fe2d92                  appstream           155 k
 perl-DBI                             x86_64           1.641-3.module+el8.4.0+509+59a8d9b3                  appstream           739 k
 perl-Math-BigInt                     noarch           1:1.9998.11-7.el8                                    baseos              194 k
 perl-Math-Complex                    noarch           1.59-420.el8                                         baseos              107 k
 psmisc                               x86_64           23.1-5.el8                                           baseos              150 k
Installing weak dependencies:
 mariadb-backup                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream           6.1 M
 mariadb-gssapi-server                x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream            50 k
 mariadb-server-utils                 x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream           1.1 M
Enabling module streams:
 mariadb                                               10.3
 perl-DBD-MySQL                                        4.046
 perl-DBI                                              1.641

Transaction Summary
======================================================================================================================================
Install  14 Packages

Total download size: 31 M
Installed size: 156 M
Is this ok [y/N]: y
Downloading Packages:
(1/14): mariadb-common-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                               333 kB/s |  62 kB     00:00
(2/14): mariadb-connector-c-3.1.11-2.el8_3.x86_64.rpm                                                 515 kB/s | 199 kB     00:00
(3/14): mariadb-connector-c-config-3.1.11-2.el8_3.noarch.rpm                                           55 kB/s |  14 kB     00:00
(4/14): mariadb-errmsg-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                               157 kB/s | 233 kB     00:01
(5/14): mariadb-gssapi-server-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                        129 kB/s |  50 kB     00:00
(6/14): mariadb-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                                      523 kB/s | 6.0 MB     00:11
(7/14): mariadb-server-utils-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                         364 kB/s | 1.1 MB     00:03
(8/14): perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64.rpm                                  34 kB/s | 155 kB     00:04
(9/14): mariadb-backup-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                               138 kB/s | 6.1 MB     00:44
(10/14): perl-Math-BigInt-1.9998.11-7.el8.noarch.rpm                                                  228 kB/s | 194 kB     00:00
(11/14): perl-Math-Complex-1.59-420.el8.noarch.rpm                                                    1.5 MB/s | 107 kB     00:00
(12/14): psmisc-23.1-5.el8.x86_64.rpm                                                                 1.5 MB/s | 150 kB     00:00
(13/14): mariadb-server-10.3.28-1.module+el8.4.0+427+adf35707.x86_64.rpm                              291 kB/s |  16 MB     00:56
[MIRROR] perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64.rpm: Curl error (28): Timeout was reached for http://mirror.in2p3.fr/pub/linux/rocky/8.5/AppStream/x86_64/os/Packages/p/perl-DBI-1.641-3.module%2bel8.4.0%2b509%2b59a8d9b3.x86_64.rpm [Operation too slow. Less than 1000 bytes/sec transferred the last 30 seconds]
(14/14): perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64.rpm                                       16 kB/s | 739 kB     00:47
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 472 kB/s |  31 MB     01:07
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                                            1/14
  Installing       : mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               2/14
  Installing       : mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               3/14
  Installing       : psmisc-23.1-5.el8.x86_64                                                                                    4/14
  Installing       : perl-Math-Complex-1.59-420.el8.noarch                                                                       5/14
  Installing       : perl-Math-BigInt-1:1.9998.11-7.el8.noarch                                                                   6/14
  Installing       : perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64                                                         7/14
  Installing       : perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64                                                   8/14
  Installing       : mariadb-connector-c-3.1.11-2.el8_3.x86_64                                                                   9/14
  Installing       : mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                              10/14
  Installing       : mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                       11/14
  Installing       : mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                        12/14
  Running scriptlet: mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                              13/14
  Installing       : mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                              13/14
  Running scriptlet: mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                              13/14
  Installing       : mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                                     14/14
  Running scriptlet: mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                                     14/14
  Verifying        : mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                                      1/14
  Verifying        : mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               2/14
  Verifying        : mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               3/14
  Verifying        : mariadb-connector-c-3.1.11-2.el8_3.x86_64                                                                   4/14
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                                            5/14
  Verifying        : mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               6/14
  Verifying        : mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                        7/14
  Verifying        : mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                               8/14
  Verifying        : mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                                         9/14
  Verifying        : perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64                                                  10/14
  Verifying        : perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64                                                        11/14
  Verifying        : perl-Math-BigInt-1:1.9998.11-7.el8.noarch                                                                  12/14
  Verifying        : perl-Math-Complex-1.59-420.el8.noarch                                                                      13/14
  Verifying        : psmisc-23.1-5.el8.x86_64                                                                                   14/14

Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64         mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                      mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64  mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64   perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64                   perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch                                 psmisc-23.1-5.el8.x86_64

Complete!
```
### Service lancé 🌞
```
[bagarre@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[bagarre@db ~]$ sudo systemctl start mariadb
[bagarre@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:02:46 CET; 6s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 2914 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 2779 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 2755 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 2882 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4943)
   Memory: 81.3M
   CGroup: /system.slice/mariadb.service
           └─2882 /usr/libexec/mysqld --basedir=/usr
```
### Firewall

```
[bagarre@db ~]$ sudo firewall-cmd --permanent  --add-port=3306/tcp
success
[bagarre@db ~]$ sudo firewall-cmd --reload
success
[bagarre@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:  80/tcp 443/tcp 1230/tcp 3306/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```
  ## Config MariaDB
  ```
  [bagarre@db ~]$ [bagarre@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y

New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] n
 ... skipping.

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
###  Préparation de la base en vue de l'utilisation par NextCloud

```

[bagarre@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'aze+123';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
### Installer commande MySQL
```
[bagarre@web ~]$ sudo dnf provides mysql
[sudo] password for bagarre:
Last metadata expiration check: 1 day, 1:00:40 ago on Mon 06 Dec 2021 02:27:17 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[bagarre@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
Last metadata expiration check: 0:00:08 ago on Tue 07 Dec 2021 03:31:33 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                               Architecture      Version                                           Repository            Size
======================================================================================================================================
Installing:
 mysql                                 x86_64            8.0.26-1.module+el8.4.0+652+6de068a7              appstream             12 M
Installing dependencies:
 mariadb-connector-c-config            noarch            3.1.11-2.el8_3                                    appstream             14 k
 mysql-common                          x86_64            8.0.26-1.module+el8.4.0+652+6de068a7              appstream            133 k
Enabling module streams:
 mysql                                                   8.0

Transaction Summary
======================================================================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): mariadb-connector-c-config-3.1.11-2.el8_3.noarch.rpm                                            32 kB/s |  14 kB     00:00
(2/3): mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                                   248 kB/s | 133 kB     00:00
(3/3): mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                                          3.7 MB/s |  12 MB     00:03
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 3.5 MB/s |  12 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                                             1/3
  Installing       : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                     2/3
  Installing       : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                            3/3
  Running scriptlet: mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                            3/3
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                                             1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                            2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                     3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                      mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```
### Test de connexion par la vm web

```
[bagarre@web ~]$ mysql -P 3306 -u nextcloud -h 10.2.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 18
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
ERROR 1046 (3D000): No database selected

```

##  Install Apache 🌞
``` 
[bagarre@web ~]$ sudo dnf provides httpd
Last metadata expiration check: 0:16:02 ago on Tue 07 Dec 2021 03:31:33 PM CET.
httpd-2.4.37-41.module+el8.5.0+695+1fa8055e.x86_64 : Apache HTTP Server
Repo        : appstream
Matched from:
Provide    : httpd = 2.4.37-41.module+el8.5.0+695+1fa8055e

httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64 : Apache HTTP Server
Repo        : appstream
Matched from:
Provide    : httpd = 2.4.37-43.module+el8.5.0+714+5ec56ee8

[bagarre@web ~]$ sudo dnf install httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
Last metadata expiration check: 0:16:52 ago on Tue 07 Dec 2021 03:31:33 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                        Architecture        Version                                              Repository              Size
======================================================================================================================================
Installing:
 httpd                          x86_64              2.4.37-43.module+el8.5.0+714+5ec56ee8                appstream              1.4 M
Installing dependencies:
 apr                            x86_64              1.6.3-12.el8                                         appstream              128 k
 apr-util                       x86_64              1.6.1-6.el8.1                                        appstream              104 k
 httpd-filesystem               noarch              2.4.37-43.module+el8.5.0+714+5ec56ee8                appstream               38 k
 httpd-tools                    x86_64              2.4.37-43.module+el8.5.0+714+5ec56ee8                appstream              106 k
 mod_http2                      x86_64              1.15.7-3.module+el8.5.0+695+1fa8055e                 appstream              153 k
 rocky-logos-httpd              noarch              85.0-3.el8                                           baseos                  22 k
Installing weak dependencies:
 apr-util-bdb                   x86_64              1.6.1-6.el8.1                                        appstream               23 k
 apr-util-openssl               x86_64              1.6.1-6.el8.1                                        appstream               26 k
Enabling module streams:
 httpd                                              2.4

Transaction Summary
======================================================================================================================================
Install  9 Packages

Total download size: 2.0 M
Installed size: 5.4 M
Is this ok [y/N]: y
Downloading Packages:
(1/9): apr-util-bdb-1.6.1-6.el8.1.x86_64.rpm                                                           82 kB/s |  23 kB     00:00
(2/9): apr-util-1.6.1-6.el8.1.x86_64.rpm                                                              237 kB/s | 104 kB     00:00
(3/9): apr-util-openssl-1.6.1-6.el8.1.x86_64.rpm                                                      167 kB/s |  26 kB     00:00
(4/9): apr-1.6.3-12.el8.x86_64.rpm                                                                    276 kB/s | 128 kB     00:00
(5/9): httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                                   844 kB/s | 106 kB     00:00
(6/9): httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch.rpm                              244 kB/s |  38 kB     00:00
(7/9): mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64.rpm                                      940 kB/s | 153 kB     00:00
(8/9): rocky-logos-httpd-85.0-3.el8.noarch.rpm                                                         80 kB/s |  22 kB     00:00
(9/9): httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                                         2.4 MB/s | 1.4 MB     00:00
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 1.3 MB/s | 2.0 MB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : apr-1.6.3-12.el8.x86_64                                                                                      1/9
  Running scriptlet: apr-1.6.3-12.el8.x86_64                                                                                      1/9
  Installing       : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                                            2/9
  Installing       : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                                        3/9
  Installing       : apr-util-1.6.1-6.el8.1.x86_64                                                                                4/9
  Running scriptlet: apr-util-1.6.1-6.el8.1.x86_64                                                                                4/9
  Installing       : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                     5/9
  Installing       : rocky-logos-httpd-85.0-3.el8.noarch                                                                          6/9
  Running scriptlet: httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                                7/9
  Installing       : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                                7/9
  Installing       : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                                        8/9
  Installing       : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                           9/9
  Running scriptlet: httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                           9/9
  Verifying        : apr-1.6.3-12.el8.x86_64                                                                                      1/9
  Verifying        : apr-util-1.6.1-6.el8.1.x86_64                                                                                2/9
  Verifying        : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                                            3/9
  Verifying        : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                                        4/9
  Verifying        : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                           5/9
  Verifying        : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                                6/9
  Verifying        : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                     7/9
  Verifying        : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                                        8/9
  Verifying        : rocky-logos-httpd-85.0-3.el8.noarch                                                                          9/9

Installed:
  apr-1.6.3-12.el8.x86_64                                         apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64                               apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64              httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64        mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!

```
### Analyse et test d'Apache 🌞
```
[bagarre@web ~]$ sudo systemctl enable httpd
[sudo] password for bagarre:
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[bagarre@web ~]$ sudo systemctl start httpd
[bagarre@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:54:31 CET; 7s ago
     Docs: man:httpd.service(8)
 Main PID: 2626 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4943)
   Memory: 25.1M
   CGroup: /system.slice/httpd.service
           ├─2626 /usr/sbin/httpd -DFOREGROUND
           ├─2627 /usr/sbin/httpd -DFOREGROUND
           ├─2628 /usr/sbin/httpd -DFOREGROUND
           ├─2629 /usr/sbin/httpd -DFOREGROUND
           └─2630 /usr/sbin/httpd -DFOREGROUND

Dec 07 15:54:30 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 15:54:31 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 15:54:31 web.tp2.cesi httpd[2626]: Server configured, listening on: port 80

[bagarre@web ~]$ sudo netstat -lntup | grep httpd
tcp6       0      0 :::80                   :::*                    LISTEN      2626/httpd

[bagarre@web ~]$ ps -ef | grep httpd
root        2626       1  0 15:54 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2627    2626  0 15:54 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2628    2626  0 15:54 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2629    2626  0 15:54 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2630    2626  0 15:54 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bagarre     2876    1534  0 16:03 pts/0    00:00:00 grep --color=auto httpd

[bagarre@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:  80/tcp 443/tcp 1230/tcp 3306/tcp
  
```
#### Test curl apache
```
[bagarre@web ~]$ curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }


  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    font-size: 2em;
  }
  h1 strong {
    font-weight: bolder;
    font-family: 'Montserrat', sans-serif;
  }
  h2 {
    font-size: 1.5em;
    font-weight:bold;
  }

  .title {
    border: 1px solid black;
    font-weight: bold;
    position: relative;
    float: right;
    width: 150px;
    text-align: center;
    padding: 10px 0 10px 0;
    margin-top: 0;
  }

  .description {
    padding: 45px 10px 5px 10px;
    clear: right;
    padding: 15px;
  }

  .section {
    padding-left: 3%;
   margin-bottom: 10px;
  }

  img {

    padding: 2px;
    margin: 2px;
  }
  a:hover img {
    padding: 2px;
    margin: 2px;
  }

  :link {
    color: rgb(199, 252, 77);
    text-shadow:
  }
  :visited {
    color: rgb(122, 206, 255);
  }
  a:hover {
    color: rgb(16, 44, 122);
  }
  .row {
    width: 100%;
    padding: 0 10px 0 10px;
  }

  footer {
    padding-top: 6em;
    margin-bottom: 6em;
    text-align: center;
    font-size: xx-small;
    overflow:hidden;
    clear: both;
  }

  .summary {
    font-size: 140%;
    text-align: center;
  }

  #rocky-poweredby img {
    margin-left: -10px;
  }

  #logos img {
    vertical-align: top;
  }

  /* Desktop  View Options */

  @media (min-width: 768px)  {

    body {
      padding: 10em 20% !important;
    }

    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6,
    .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }

    .col-md-1 {
      width: 8.33%;
    }
    .col-md-2 {
      width: 16.66%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-4 {
      width: 33%;
    }
    .col-md-5 {
      width: 41.66%;
    }
    .col-md-6 {
      border-left:3px ;
      width: 50%;


    }
    .col-md-7 {
      width: 58.33%;
    }
    .col-md-8 {
      width: 66.66%;
    }
    .col-md-9 {
      width: 74.99%;
    }
    .col-md-10 {
      width: 83.33%;
    }
    .col-md-11 {
      width: 91.66%;
    }
    .col-md-12 {
      width: 100%;
    }
  }

  /* Mobile View Options */
  @media (max-width: 767px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }

    .col-sm-1 {
      width: 8.33%;
    }
    .col-sm-2 {
      width: 16.66%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-4 {
      width: 33%;
    }
    .col-sm-5 {
      width: 41.66%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-7 {
      width: 58.33%;
    }
    .col-sm-8 {
      width: 66.66%;
    }
    .col-sm-9 {
      width: 74.99%;
    }
    .col-sm-10 {
      width: 83.33%;
    }
    .col-sm-11 {
      width: 91.66%;
    }
    .col-sm-12 {
      width: 100%;
    }
    h1 {
      padding: 0 !important;
    }
  }


  </style>
  </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src= "icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>

```
### PHP 
``` 
[bagarre@web ~]$ sudo dnf install epel-release
[sudo] password for bagarre:
Last metadata expiration check: 0:39:49 ago on Tue 07 Dec 2021 03:31:33 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                            Architecture                 Version                           Repository                    Size
======================================================================================================================================
Installing:
 epel-release                       noarch                       8-13.el8                          extras                        23 k

Transaction Summary
======================================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                                      112 kB/s |  23 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                  44 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : epel-release-8-13.el8.noarch                                                                                 1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                                 1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                                 1/1

Installed:
  epel-release-8-13.el8.noarch
Complete!
```
#### DNF upgrade
``` 
[bagarre@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                                        1.7 MB/s |  11 MB     00:06
Extra Packages for Enterprise Linux Modular 8 - x86_64                                                845 kB/s | 980 kB     00:01
Dependencies resolved.
======================================================================================================================================
 Package                             Architecture            Version                                 Repository                  Size
======================================================================================================================================
Installing:
 kernel                              x86_64                  4.18.0-348.2.1.el8_5                    baseos                     7.0 M
Upgrading:
 binutils                            x86_64                  2.30-108.el8_5.1                        baseos                     5.8 M
 bpftool                             x86_64                  4.18.0-348.2.1.el8_5                    baseos                     7.7 M
 kernel-tools                        x86_64                  4.18.0-348.2.1.el8_5                    baseos                     7.2 M
 kernel-tools-libs                   x86_64                  4.18.0-348.2.1.el8_5                    baseos                     7.0 M
 libgcc                              x86_64                  8.5.0-4.el8_5                           baseos                      78 k
 libgomp                             x86_64                  8.5.0-4.el8_5                           baseos                     205 k
 libipa_hbac                         x86_64                  2.5.2-2.el8_5.1                         baseos                     114 k
 libsss_autofs                       x86_64                  2.5.2-2.el8_5.1                         baseos                     116 k
 libsss_certmap                      x86_64                  2.5.2-2.el8_5.1                         baseos                     154 k
 libsss_idmap                        x86_64                  2.5.2-2.el8_5.1                         baseos                     119 k
 libsss_nss_idmap                    x86_64                  2.5.2-2.el8_5.1                         baseos                     125 k
 libsss_sudo                         x86_64                  2.5.2-2.el8_5.1                         baseos                     115 k
 libstdc++                           x86_64                  8.5.0-4.el8_5                           baseos                     452 k
 python3-perf                        x86_64                  4.18.0-348.2.1.el8_5                    baseos                     7.1 M
 python3-sssdconfig                  noarch                  2.5.2-2.el8_5.1                         baseos                     141 k
 sssd                                x86_64                  2.5.2-2.el8_5.1                         baseos                     105 k
 sssd-ad                             x86_64                  2.5.2-2.el8_5.1                         baseos                     269 k
 sssd-client                         x86_64                  2.5.2-2.el8_5.1                         baseos                     204 k
 sssd-common                         x86_64                  2.5.2-2.el8_5.1                         baseos                     1.6 M
 sssd-common-pac                     x86_64                  2.5.2-2.el8_5.1                         baseos                     177 k
 sssd-ipa                            x86_64                  2.5.2-2.el8_5.1                         baseos                     346 k
 sssd-kcm                            x86_64                  2.5.2-2.el8_5.1                         baseos                     253 k
 sssd-krb5                           x86_64                  2.5.2-2.el8_5.1                         baseos                     148 k
 sssd-krb5-common                    x86_64                  2.5.2-2.el8_5.1                         baseos                     184 k
 sssd-ldap                           x86_64                  2.5.2-2.el8_5.1                         baseos                     207 k
 sssd-nfs-idmap                      x86_64                  2.5.2-2.el8_5.1                         baseos                     114 k
 sssd-proxy                          x86_64                  2.5.2-2.el8_5.1                         baseos                     146 k
 unzip                               x86_64                  6.0-45.el8_4                            baseos                     194 k
Installing dependencies:
 bind-libs                           x86_64                  32:9.11.26-6.el8                        appstream                  173 k
 bind-libs-lite                      x86_64                  32:9.11.26-6.el8                        appstream                  1.2 M
 bind-license                        noarch                  32:9.11.26-6.el8                        appstream                  101 k
 fstrm                               x86_64                  0.6.1-2.el8                             appstream                   28 k
 kernel-core                         x86_64                  4.18.0-348.2.1.el8_5                    baseos                      38 M
 kernel-modules                      x86_64                  4.18.0-348.2.1.el8_5                    baseos                      30 M
 libmaxminddb                        x86_64                  1.2.0-10.el8                            appstream                   32 k
 protobuf-c                          x86_64                  1.3.0-6.el8                             appstream                   36 k
 python3-bind                        noarch                  32:9.11.26-6.el8                        appstream                  149 k
 python3-ply                         noarch                  3.9-9.el8                               baseos                     110 k
Installing weak dependencies:
 bind-utils                          x86_64                  32:9.11.26-6.el8                        appstream                  450 k
 geolite2-city                       noarch                  20180605-1.el8                          appstream                   19 M
 geolite2-country                    noarch                  20180605-1.el8                          appstream                  1.0 M

Transaction Summary
======================================================================================================================================
Install  14 Packages
Upgrade  28 Packages

Total download size: 137 M
Is this ok [y/N]: y
Downloading Packages:
(1/42): bind-license-9.11.26-6.el8.noarch.rpm                                                         638 kB/s | 101 kB     00:00
(2/42): bind-libs-9.11.26-6.el8.x86_64.rpm                                                            890 kB/s | 173 kB     00:00
(3/42): fstrm-0.6.1-2.el8.x86_64.rpm                                                                  732 kB/s |  28 kB     00:00
(4/42): bind-utils-9.11.26-6.el8.x86_64.rpm                                                           1.5 MB/s | 450 kB     00:00
(5/42): bind-libs-lite-9.11.26-6.el8.x86_64.rpm                                                       1.5 MB/s | 1.2 MB     00:00
(6/42): libmaxminddb-1.2.0-10.el8.x86_64.rpm                                                          721 kB/s |  32 kB     00:00
(7/42): protobuf-c-1.3.0-6.el8.x86_64.rpm                                                             344 kB/s |  36 kB     00:00
(8/42): geolite2-country-20180605-1.el8.noarch.rpm                                                    1.7 MB/s | 1.0 MB     00:00
(9/42): python3-bind-9.11.26-6.el8.noarch.rpm                                                         812 kB/s | 149 kB     00:00
(10/42): kernel-4.18.0-348.2.1.el8_5.x86_64.rpm                                                       2.0 MB/s | 7.0 MB     00:03
(11/42): geolite2-city-20180605-1.el8.noarch.rpm                                                      2.0 MB/s |  19 MB     00:09
(12/42): python3-ply-3.9-9.el8.noarch.rpm                                                             229 kB/s | 110 kB     00:00
(13/42): binutils-2.30-108.el8_5.1.x86_64.rpm                                                         1.8 MB/s | 5.8 MB     00:03
(14/42): bpftool-4.18.0-348.2.1.el8_5.x86_64.rpm                                                      2.3 MB/s | 7.7 MB     00:03
(15/42): kernel-tools-4.18.0-348.2.1.el8_5.x86_64.rpm                                                 1.6 MB/s | 7.2 MB     00:04
(16/42): kernel-modules-4.18.0-348.2.1.el8_5.x86_64.rpm                                               1.6 MB/s |  30 MB     00:18
(17/42): libgcc-8.5.0-4.el8_5.x86_64.rpm                                                              267 kB/s |  78 kB     00:00
(18/42): libgomp-8.5.0-4.el8_5.x86_64.rpm                                                             467 kB/s | 205 kB     00:00
(19/42): libipa_hbac-2.5.2-2.el8_5.1.x86_64.rpm                                                       389 kB/s | 114 kB     00:00
(20/42): libsss_autofs-2.5.2-2.el8_5.1.x86_64.rpm                                                     585 kB/s | 116 kB     00:00
(21/42): kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64.rpm                                            1.8 MB/s | 7.0 MB     00:03
(22/42): libsss_certmap-2.5.2-2.el8_5.1.x86_64.rpm                                                    396 kB/s | 154 kB     00:00
(23/42): libsss_idmap-2.5.2-2.el8_5.1.x86_64.rpm                                                      254 kB/s | 119 kB     00:00
(24/42): libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64.rpm                                                  432 kB/s | 125 kB     00:00
(25/42): libsss_sudo-2.5.2-2.el8_5.1.x86_64.rpm                                                       686 kB/s | 115 kB     00:00
(26/42): libstdc++-8.5.0-4.el8_5.x86_64.rpm                                                           1.6 MB/s | 452 kB     00:00
(27/42): python3-sssdconfig-2.5.2-2.el8_5.1.noarch.rpm                                                628 kB/s | 141 kB     00:00
(28/42): sssd-2.5.2-2.el8_5.1.x86_64.rpm                                                              412 kB/s | 105 kB     00:00
(29/42): sssd-ad-2.5.2-2.el8_5.1.x86_64.rpm                                                           1.9 MB/s | 269 kB     00:00
(30/42): sssd-client-2.5.2-2.el8_5.1.x86_64.rpm                                                       2.0 MB/s | 204 kB     00:00
(31/42): sssd-common-2.5.2-2.el8_5.1.x86_64.rpm                                                       2.5 MB/s | 1.6 MB     00:00
(32/42): sssd-common-pac-2.5.2-2.el8_5.1.x86_64.rpm                                                   906 kB/s | 177 kB     00:00
(33/42): sssd-ipa-2.5.2-2.el8_5.1.x86_64.rpm                                                          867 kB/s | 346 kB     00:00
(34/42): sssd-kcm-2.5.2-2.el8_5.1.x86_64.rpm                                                          648 kB/s | 253 kB     00:00
(35/42): sssd-krb5-2.5.2-2.el8_5.1.x86_64.rpm                                                         871 kB/s | 148 kB     00:00
(36/42): sssd-krb5-common-2.5.2-2.el8_5.1.x86_64.rpm                                                  648 kB/s | 184 kB     00:00
(37/42): sssd-ldap-2.5.2-2.el8_5.1.x86_64.rpm                                                         351 kB/s | 207 kB     00:00
(38/42): sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64.rpm                                                    435 kB/s | 114 kB     00:00
(39/42): python3-perf-4.18.0-348.2.1.el8_5.x86_64.rpm                                                 1.8 MB/s | 7.1 MB     00:03
(40/42): sssd-proxy-2.5.2-2.el8_5.1.x86_64.rpm                                                        936 kB/s | 146 kB     00:00
(41/42): unzip-6.0-45.el8_4.x86_64.rpm                                                                970 kB/s | 194 kB     00:00
(42/42): kernel-core-4.18.0-348.2.1.el8_5.x86_64.rpm                                                  1.0 MB/s |  38 MB     00:36
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 3.6 MB/s | 137 MB     00:37
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Running scriptlet: libsss_idmap-2.5.2-2.el8_5.1.x86_64                                                                          1/1
  Upgrading        : libsss_idmap-2.5.2-2.el8_5.1.x86_64                                                                         1/70
  Running scriptlet: libsss_idmap-2.5.2-2.el8_5.1.x86_64                                                                         1/70
  Upgrading        : libsss_certmap-2.5.2-2.el8_5.1.x86_64                                                                       2/70
  Running scriptlet: libsss_certmap-2.5.2-2.el8_5.1.x86_64                                                                       2/70
  Installing       : protobuf-c-1.3.0-6.el8.x86_64                                                                               3/70
  Installing       : fstrm-0.6.1-2.el8.x86_64                                                                                    4/70
  Installing       : bind-license-32:9.11.26-6.el8.noarch                                                                        5/70
  Upgrading        : libgcc-8.5.0-4.el8_5.x86_64                                                                                 6/70
  Running scriptlet: libgcc-8.5.0-4.el8_5.x86_64                                                                                 6/70
  Installing       : kernel-core-4.18.0-348.2.1.el8_5.x86_64                                                                     7/70
  Running scriptlet: kernel-core-4.18.0-348.2.1.el8_5.x86_64                                                                     7/70
  Installing       : kernel-modules-4.18.0-348.2.1.el8_5.x86_64                                                                  8/70
  Running scriptlet: kernel-modules-4.18.0-348.2.1.el8_5.x86_64                                                                  8/70
  Upgrading        : libstdc++-8.5.0-4.el8_5.x86_64                                                                              9/70
  Running scriptlet: libstdc++-8.5.0-4.el8_5.x86_64                                                                              9/70
  Upgrading        : sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64                                                                      10/70
  Upgrading        : python3-sssdconfig-2.5.2-2.el8_5.1.noarch                                                                  11/70
  Upgrading        : libsss_sudo-2.5.2-2.el8_5.1.x86_64                                                                         12/70
  Running scriptlet: libsss_sudo-2.5.2-2.el8_5.1.x86_64                                                                         12/70
  Upgrading        : libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                                                                    13/70
  Running scriptlet: libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                                                                    13/70
  Upgrading        : sssd-client-2.5.2-2.el8_5.1.x86_64                                                                         14/70
  Running scriptlet: sssd-client-2.5.2-2.el8_5.1.x86_64                                                                         14/70
  Upgrading        : libsss_autofs-2.5.2-2.el8_5.1.x86_64                                                                       15/70
  Running scriptlet: sssd-common-2.5.2-2.el8_5.1.x86_64                                                                         16/70
  Upgrading        : sssd-common-2.5.2-2.el8_5.1.x86_64                                                                         16/70
  Running scriptlet: sssd-common-2.5.2-2.el8_5.1.x86_64                                                                         16/70
  Running scriptlet: sssd-krb5-common-2.5.2-2.el8_5.1.x86_64                                                                    17/70
  Upgrading        : sssd-krb5-common-2.5.2-2.el8_5.1.x86_64                                                                    17/70
  Upgrading        : sssd-common-pac-2.5.2-2.el8_5.1.x86_64                                                                     18/70
  Upgrading        : sssd-krb5-2.5.2-2.el8_5.1.x86_64                                                                           19/70
  Upgrading        : sssd-ldap-2.5.2-2.el8_5.1.x86_64                                                                           20/70
  Running scriptlet: sssd-proxy-2.5.2-2.el8_5.1.x86_64                                                                          21/70
  Upgrading        : sssd-proxy-2.5.2-2.el8_5.1.x86_64                                                                          21/70
  Upgrading        : libipa_hbac-2.5.2-2.el8_5.1.x86_64                                                                         22/70
  Running scriptlet: libipa_hbac-2.5.2-2.el8_5.1.x86_64                                                                         22/70
  Upgrading        : kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64                                                              23/70
  Running scriptlet: kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64                                                              23/70
  Installing       : python3-ply-3.9-9.el8.noarch                                                                               24/70
  Installing       : python3-bind-32:9.11.26-6.el8.noarch                                                                       25/70
  Installing       : geolite2-country-20180605-1.el8.noarch                                                                     26/70
  Installing       : geolite2-city-20180605-1.el8.noarch                                                                        27/70
  Installing       : libmaxminddb-1.2.0-10.el8.x86_64                                                                           28/70
  Running scriptlet: libmaxminddb-1.2.0-10.el8.x86_64                                                                           28/70
  Installing       : bind-libs-lite-32:9.11.26-6.el8.x86_64                                                                     29/70
  Installing       : bind-libs-32:9.11.26-6.el8.x86_64                                                                          30/70
  Installing       : bind-utils-32:9.11.26-6.el8.x86_64                                                                         31/70
  Upgrading        : sssd-ad-2.5.2-2.el8_5.1.x86_64                                                                             32/70
  Running scriptlet: sssd-ipa-2.5.2-2.el8_5.1.x86_64                                                                            33/70
  Upgrading        : sssd-ipa-2.5.2-2.el8_5.1.x86_64                                                                            33/70
  Upgrading        : sssd-2.5.2-2.el8_5.1.x86_64                                                                                34/70
  Upgrading        : kernel-tools-4.18.0-348.2.1.el8_5.x86_64                                                                   35/70
  Upgrading        : sssd-kcm-2.5.2-2.el8_5.1.x86_64                                                                            36/70
  Running scriptlet: sssd-kcm-2.5.2-2.el8_5.1.x86_64                                                                            36/70
  Upgrading        : binutils-2.30-108.el8_5.1.x86_64                                                                           37/70
  Running scriptlet: binutils-2.30-108.el8_5.1.x86_64                                                                           37/70
  Installing       : kernel-4.18.0-348.2.1.el8_5.x86_64                                                                         38/70
  Upgrading        : unzip-6.0-45.el8_4.x86_64                                                                                  39/70
  Upgrading        : python3-perf-4.18.0-348.2.1.el8_5.x86_64                                                                   40/70
  Upgrading        : libgomp-8.5.0-4.el8_5.x86_64                                                                               41/70
  Running scriptlet: libgomp-8.5.0-4.el8_5.x86_64                                                                               41/70
  Upgrading        : bpftool-4.18.0-348.2.1.el8_5.x86_64                                                                        42/70
  Running scriptlet: binutils-2.30-108.el8.x86_64                                                                               43/70
  Cleanup          : binutils-2.30-108.el8.x86_64                                                                               43/70
  Running scriptlet: binutils-2.30-108.el8.x86_64                                                                               43/70
  Cleanup          : sssd-2.5.2-2.el8.x86_64                                                                                    44/70
  Cleanup          : sssd-ipa-2.5.2-2.el8.x86_64                                                                                45/70
  Cleanup          : sssd-ad-2.5.2-2.el8.x86_64                                                                                 46/70
  Cleanup          : sssd-ldap-2.5.2-2.el8.x86_64                                                                               47/70
  Cleanup          : sssd-common-pac-2.5.2-2.el8.x86_64                                                                         48/70
  Cleanup          : sssd-krb5-2.5.2-2.el8.x86_64                                                                               49/70
  Cleanup          : sssd-proxy-2.5.2-2.el8.x86_64                                                                              50/70
  Running scriptlet: sssd-kcm-2.5.2-2.el8.x86_64                                                                                51/70
  Cleanup          : sssd-kcm-2.5.2-2.el8.x86_64                                                                                51/70
  Running scriptlet: sssd-kcm-2.5.2-2.el8.x86_64                                                                                51/70
  Cleanup          : libstdc++-8.5.0-3.el8.x86_64                                                                               52/70
  Running scriptlet: libstdc++-8.5.0-3.el8.x86_64                                                                               52/70
  Cleanup          : sssd-krb5-common-2.5.2-2.el8.x86_64                                                                        53/70
  Running scriptlet: sssd-common-2.5.2-2.el8.x86_64                                                                             54/70
  Cleanup          : sssd-common-2.5.2-2.el8.x86_64                                                                             54/70
  Running scriptlet: sssd-common-2.5.2-2.el8.x86_64                                                                             54/70
  Running scriptlet: sssd-client-2.5.2-2.el8.x86_64                                                                             55/70
  Cleanup          : sssd-client-2.5.2-2.el8.x86_64                                                                             55/70
  Running scriptlet: sssd-client-2.5.2-2.el8.x86_64                                                                             55/70
  Cleanup          : kernel-tools-4.18.0-348.el8.0.2.x86_64                                                                     56/70
  Cleanup          : python3-sssdconfig-2.5.2-2.el8.noarch                                                                      57/70
  Cleanup          : kernel-tools-libs-4.18.0-348.el8.0.2.x86_64                                                                58/70
  Running scriptlet: kernel-tools-libs-4.18.0-348.el8.0.2.x86_64                                                                58/70
  Cleanup          : libsss_idmap-2.5.2-2.el8.x86_64                                                                            59/70
  Running scriptlet: libsss_idmap-2.5.2-2.el8.x86_64                                                                            59/70
  Cleanup          : libsss_nss_idmap-2.5.2-2.el8.x86_64                                                                        60/70
  Running scriptlet: libsss_nss_idmap-2.5.2-2.el8.x86_64                                                                        60/70
  Cleanup          : libsss_certmap-2.5.2-2.el8.x86_64                                                                          61/70
  Running scriptlet: libsss_certmap-2.5.2-2.el8.x86_64                                                                          61/70
  Cleanup          : libsss_autofs-2.5.2-2.el8.x86_64                                                                           62/70
  Cleanup          : libsss_sudo-2.5.2-2.el8.x86_64                                                                             63/70
  Running scriptlet: libsss_sudo-2.5.2-2.el8.x86_64                                                                             63/70
  Cleanup          : sssd-nfs-idmap-2.5.2-2.el8.x86_64                                                                          64/70
  Cleanup          : libgcc-8.5.0-3.el8.x86_64                                                                                  65/70
  Running scriptlet: libgcc-8.5.0-3.el8.x86_64                                                                                  65/70
  Cleanup          : libipa_hbac-2.5.2-2.el8.x86_64                                                                             66/70
  Running scriptlet: libipa_hbac-2.5.2-2.el8.x86_64                                                                             66/70
  Cleanup          : unzip-6.0-45.el8.x86_64                                                                                    67/70
  Cleanup          : python3-perf-4.18.0-348.el8.0.2.x86_64                                                                     68/70
  Running scriptlet: libgomp-8.5.0-3.el8.x86_64                                                                                 69/70
  Cleanup          : libgomp-8.5.0-3.el8.x86_64                                                                                 69/70
  Running scriptlet: libgomp-8.5.0-3.el8.x86_64                                                                                 69/70
  Cleanup          : bpftool-4.18.0-348.el8.0.2.x86_64                                                                          70/70
  Running scriptlet: kernel-core-4.18.0-348.2.1.el8_5.x86_64                                                                    70/70
  Running scriptlet: sssd-common-2.5.2-2.el8_5.1.x86_64                                                                         70/70
  Running scriptlet: bpftool-4.18.0-348.el8.0.2.x86_64                                                                          70/70
  Verifying        : bind-libs-32:9.11.26-6.el8.x86_64                                                                           1/70
  Verifying        : bind-libs-lite-32:9.11.26-6.el8.x86_64                                                                      2/70
  Verifying        : bind-license-32:9.11.26-6.el8.noarch                                                                        3/70
  Verifying        : bind-utils-32:9.11.26-6.el8.x86_64                                                                          4/70
  Verifying        : fstrm-0.6.1-2.el8.x86_64                                                                                    5/70
  Verifying        : geolite2-city-20180605-1.el8.noarch                                                                         6/70
  Verifying        : geolite2-country-20180605-1.el8.noarch                                                                      7/70
  Verifying        : libmaxminddb-1.2.0-10.el8.x86_64                                                                            8/70
  Verifying        : protobuf-c-1.3.0-6.el8.x86_64                                                                               9/70
  Verifying        : python3-bind-32:9.11.26-6.el8.noarch                                                                       10/70
  Verifying        : kernel-4.18.0-348.2.1.el8_5.x86_64                                                                         11/70
  Verifying        : kernel-core-4.18.0-348.2.1.el8_5.x86_64                                                                    12/70
  Verifying        : kernel-modules-4.18.0-348.2.1.el8_5.x86_64                                                                 13/70
  Verifying        : python3-ply-3.9-9.el8.noarch                                                                               14/70
  Verifying        : binutils-2.30-108.el8_5.1.x86_64                                                                           15/70
  Verifying        : binutils-2.30-108.el8.x86_64                                                                               16/70
  Verifying        : bpftool-4.18.0-348.2.1.el8_5.x86_64                                                                        17/70
  Verifying        : bpftool-4.18.0-348.el8.0.2.x86_64                                                                          18/70
  Verifying        : kernel-tools-4.18.0-348.2.1.el8_5.x86_64                                                                   19/70
  Verifying        : kernel-tools-4.18.0-348.el8.0.2.x86_64                                                                     20/70
  Verifying        : kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64                                                              21/70
  Verifying        : kernel-tools-libs-4.18.0-348.el8.0.2.x86_64                                                                22/70
  Verifying        : libgcc-8.5.0-4.el8_5.x86_64                                                                                23/70
  Verifying        : libgcc-8.5.0-3.el8.x86_64                                                                                  24/70
  Verifying        : libgomp-8.5.0-4.el8_5.x86_64                                                                               25/70
  Verifying        : libgomp-8.5.0-3.el8.x86_64                                                                                 26/70
  Verifying        : libipa_hbac-2.5.2-2.el8_5.1.x86_64                                                                         27/70
  Verifying        : libipa_hbac-2.5.2-2.el8.x86_64                                                                             28/70
  Verifying        : libsss_autofs-2.5.2-2.el8_5.1.x86_64                                                                       29/70
  Verifying        : libsss_autofs-2.5.2-2.el8.x86_64                                                                           30/70
  Verifying        : libsss_certmap-2.5.2-2.el8_5.1.x86_64                                                                      31/70
  Verifying        : libsss_certmap-2.5.2-2.el8.x86_64                                                                          32/70
  Verifying        : libsss_idmap-2.5.2-2.el8_5.1.x86_64                                                                        33/70
  Verifying        : libsss_idmap-2.5.2-2.el8.x86_64                                                                            34/70
  Verifying        : libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                                                                    35/70
  Verifying        : libsss_nss_idmap-2.5.2-2.el8.x86_64                                                                        36/70
  Verifying        : libsss_sudo-2.5.2-2.el8_5.1.x86_64                                                                         37/70
  Verifying        : libsss_sudo-2.5.2-2.el8.x86_64                                                                             38/70
  Verifying        : libstdc++-8.5.0-4.el8_5.x86_64                                                                             39/70
  Verifying        : libstdc++-8.5.0-3.el8.x86_64                                                                               40/70
  Verifying        : python3-perf-4.18.0-348.2.1.el8_5.x86_64                                                                   41/70
  Verifying        : python3-perf-4.18.0-348.el8.0.2.x86_64                                                                     42/70
  Verifying        : python3-sssdconfig-2.5.2-2.el8_5.1.noarch                                                                  43/70
  Verifying        : python3-sssdconfig-2.5.2-2.el8.noarch                                                                      44/70
  Verifying        : sssd-2.5.2-2.el8_5.1.x86_64                                                                                45/70
  Verifying        : sssd-2.5.2-2.el8.x86_64                                                                                    46/70
  Verifying        : sssd-ad-2.5.2-2.el8_5.1.x86_64                                                                             47/70
  Verifying        : sssd-ad-2.5.2-2.el8.x86_64                                                                                 48/70
  Verifying        : sssd-client-2.5.2-2.el8_5.1.x86_64                                                                         49/70
  Verifying        : sssd-client-2.5.2-2.el8.x86_64                                                                             50/70
  Verifying        : sssd-common-2.5.2-2.el8_5.1.x86_64                                                                         51/70
  Verifying        : sssd-common-2.5.2-2.el8.x86_64                                                                             52/70
  Verifying        : sssd-common-pac-2.5.2-2.el8_5.1.x86_64                                                                     53/70
  Verifying        : sssd-common-pac-2.5.2-2.el8.x86_64                                                                         54/70
  Verifying        : sssd-ipa-2.5.2-2.el8_5.1.x86_64                                                                            55/70
  Verifying        : sssd-ipa-2.5.2-2.el8.x86_64                                                                                56/70
  Verifying        : sssd-kcm-2.5.2-2.el8_5.1.x86_64                                                                            57/70
  Verifying        : sssd-kcm-2.5.2-2.el8.x86_64                                                                                58/70
  Verifying        : sssd-krb5-2.5.2-2.el8_5.1.x86_64                                                                           59/70
  Verifying        : sssd-krb5-2.5.2-2.el8.x86_64                                                                               60/70
  Verifying        : sssd-krb5-common-2.5.2-2.el8_5.1.x86_64                                                                    61/70
  Verifying        : sssd-krb5-common-2.5.2-2.el8.x86_64                                                                        62/70
  Verifying        : sssd-ldap-2.5.2-2.el8_5.1.x86_64                                                                           63/70
  Verifying        : sssd-ldap-2.5.2-2.el8.x86_64                                                                               64/70
  Verifying        : sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64                                                                      65/70
  Verifying        : sssd-nfs-idmap-2.5.2-2.el8.x86_64                                                                          66/70
  Verifying        : sssd-proxy-2.5.2-2.el8_5.1.x86_64                                                                          67/70
  Verifying        : sssd-proxy-2.5.2-2.el8.x86_64                                                                              68/70
  Verifying        : unzip-6.0-45.el8_4.x86_64                                                                                  69/70
  Verifying        : unzip-6.0-45.el8.x86_64                                                                                    70/70

Upgraded:
  binutils-2.30-108.el8_5.1.x86_64               bpftool-4.18.0-348.2.1.el8_5.x86_64       kernel-tools-4.18.0-348.2.1.el8_5.x86_64
  kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64  libgcc-8.5.0-4.el8_5.x86_64               libgomp-8.5.0-4.el8_5.x86_64
  libipa_hbac-2.5.2-2.el8_5.1.x86_64             libsss_autofs-2.5.2-2.el8_5.1.x86_64      libsss_certmap-2.5.2-2.el8_5.1.x86_64
  libsss_idmap-2.5.2-2.el8_5.1.x86_64            libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64   libsss_sudo-2.5.2-2.el8_5.1.x86_64
  libstdc++-8.5.0-4.el8_5.x86_64                 python3-perf-4.18.0-348.2.1.el8_5.x86_64  python3-sssdconfig-2.5.2-2.el8_5.1.noarch
  sssd-2.5.2-2.el8_5.1.x86_64                    sssd-ad-2.5.2-2.el8_5.1.x86_64            sssd-client-2.5.2-2.el8_5.1.x86_64
  sssd-common-2.5.2-2.el8_5.1.x86_64             sssd-common-pac-2.5.2-2.el8_5.1.x86_64    sssd-ipa-2.5.2-2.el8_5.1.x86_64
  sssd-kcm-2.5.2-2.el8_5.1.x86_64                sssd-krb5-2.5.2-2.el8_5.1.x86_64          sssd-krb5-common-2.5.2-2.el8_5.1.x86_64
  sssd-ldap-2.5.2-2.el8_5.1.x86_64               sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64     sssd-proxy-2.5.2-2.el8_5.1.x86_64
  unzip-6.0-45.el8_4.x86_64
Installed:
  bind-libs-32:9.11.26-6.el8.x86_64             bind-libs-lite-32:9.11.26-6.el8.x86_64    bind-license-32:9.11.26-6.el8.noarch
  bind-utils-32:9.11.26-6.el8.x86_64            fstrm-0.6.1-2.el8.x86_64                  geolite2-city-20180605-1.el8.noarch
  geolite2-country-20180605-1.el8.noarch        kernel-4.18.0-348.2.1.el8_5.x86_64        kernel-core-4.18.0-348.2.1.el8_5.x86_64
  kernel-modules-4.18.0-348.2.1.el8_5.x86_64    libmaxminddb-1.2.0-10.el8.x86_64          protobuf-c-1.3.0-6.el8.x86_64
  python3-bind-32:9.11.26-6.el8.noarch          python3-ply-3.9-9.el8.noarch

Complete!

```
#### Ajout dépôts REMI

```
`[bagarre@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:08:48 ago on Tue 07 Dec 2021 04:11:39 PM CET.
remi-release-8.rpm                                                                                     73 kB/s |  26 kB     00:00
Dependencies resolved.
======================================================================================================================================
 Package                         Architecture              Version                              Repository                       Size
======================================================================================================================================
Installing:
 remi-release                    noarch                    8.5-2.el8.remi                       @commandline                     26 k

Transaction Summary
======================================================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                                           1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                                           1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

```
#### Activation php remi

```
[bagarre@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             2.2 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             1.2 MB/s | 946 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            3.0 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            733 kB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            1.6 MB/s | 2.0 MB     00:01
Dependencies resolved.
======================================================================================================================================
 Package                         Architecture                   Version                         Repository                       Size
======================================================================================================================================
Enabling module streams:
 php                                                            remi-7.4

Transaction Summary
======================================================================================================================================

Is this ok [y/N]: y
Complete!

```
####Install php + libs

```
[bagarre@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:01:12 ago on Tue 07 Dec 2021 04:21:18 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
======================================================================================================================================
 Package                                 Architecture      Version                                         Repository            Size
======================================================================================================================================
Installing:
 php74-php                               x86_64            7.4.26-1.el8.remi                               remi-safe            1.5 M
 php74-php-bcmath                        x86_64            7.4.26-1.el8.remi                               remi-safe             88 k
 php74-php-common                        x86_64            7.4.26-1.el8.remi                               remi-safe            710 k
 php74-php-gd                            x86_64            7.4.26-1.el8.remi                               remi-safe             93 k
 php74-php-gmp                           x86_64            7.4.26-1.el8.remi                               remi-safe             84 k
 php74-php-intl                          x86_64            7.4.26-1.el8.remi                               remi-safe            201 k
 php74-php-json                          x86_64            7.4.26-1.el8.remi                               remi-safe             82 k
 php74-php-mbstring                      x86_64            7.4.26-1.el8.remi                               remi-safe            492 k
 php74-php-mysqlnd                       x86_64            7.4.26-1.el8.remi                               remi-safe            200 k
 php74-php-pdo                           x86_64            7.4.26-1.el8.remi                               remi-safe            130 k
 php74-php-pecl-zip                      x86_64            1.20.0-1.el8.remi                               remi-safe             58 k
 php74-php-process                       x86_64            7.4.26-1.el8.remi                               remi-safe             92 k
 php74-php-xml                           x86_64            7.4.26-1.el8.remi                               remi-safe            180 k
Installing dependencies:
 checkpolicy                             x86_64            2.9-1.el8                                       baseos               345 k
 environment-modules                     x86_64            4.5.2-1.el8                                     baseos               420 k
 libicu69                                x86_64            69.1-1.el8.remi                                 remi-safe            9.6 M
 libsodium                               x86_64            1.0.18-2.el8                                    epel                 162 k
 oniguruma5php                           x86_64            6.9.7.1-1.el8.remi                              remi-safe            210 k
 php74-libzip                            x86_64            1.8.0-1.el8.remi                                remi-safe             69 k
 php74-runtime                           x86_64            1.0-3.el8.remi                                  remi-safe            1.1 M
 policycoreutils-python-utils            noarch            2.9-16.el8                                      baseos               251 k
 python3-audit                           x86_64            3.0-0.17.20191104git1c2f876.el8.1               baseos                85 k
 python3-libsemanage                     x86_64            2.9-6.el8                                       baseos               126 k
 python3-policycoreutils                 noarch            2.9-16.el8                                      baseos               2.2 M
 python3-setools                         x86_64            4.3.0-2.el8                                     baseos               625 k
 scl-utils                               x86_64            1:2.0.2-14.el8                                  appstream             46 k
 tcl                                     x86_64            1:8.6.8-2.el8                                   baseos               1.1 M
Installing weak dependencies:
 php74-php-cli                           x86_64            7.4.26-1.el8.remi                               remi-safe            3.1 M
 php74-php-fpm                           x86_64            7.4.26-1.el8.remi                               remi-safe            1.6 M
 php74-php-opcache                       x86_64            7.4.26-1.el8.remi                               remi-safe            275 k
 php74-php-sodium                        x86_64            7.4.26-1.el8.remi                               remi-safe             87 k

Transaction Summary
======================================================================================================================================
Install  31 Packages

Total download size: 25 M
Installed size: 84 M
Is this ok [y/N]: y
Downloading Packages:
(1/31): scl-utils-2.0.2-14.el8.x86_64.rpm                                                             329 kB/s |  46 kB     00:00
(2/31): checkpolicy-2.9-1.el8.x86_64.rpm                                                              787 kB/s | 345 kB     00:00
(3/31): environment-modules-4.5.2-1.el8.x86_64.rpm                                                    856 kB/s | 420 kB     00:00
(4/31): python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64.rpm                                    1.4 MB/s |  85 kB     00:00
(5/31): python3-libsemanage-2.9-6.el8.x86_64.rpm                                                      1.6 MB/s | 126 kB     00:00
(6/31): policycoreutils-python-utils-2.9-16.el8.noarch.rpm                                            474 kB/s | 251 kB     00:00
(7/31): python3-setools-4.3.0-2.el8.x86_64.rpm                                                        1.2 MB/s | 625 kB     00:00
(8/31): libsodium-1.0.18-2.el8.x86_64.rpm                                                             637 kB/s | 162 kB     00:00
(9/31): tcl-8.6.8-2.el8.x86_64.rpm                                                                    1.7 MB/s | 1.1 MB     00:00
(10/31): python3-policycoreutils-2.9-16.el8.noarch.rpm                                                2.3 MB/s | 2.2 MB     00:00
(11/31): oniguruma5php-6.9.7.1-1.el8.remi.x86_64.rpm                                                  487 kB/s | 210 kB     00:00
(12/31): php74-libzip-1.8.0-1.el8.remi.x86_64.rpm                                                     222 kB/s |  69 kB     00:00
(13/31): php74-php-bcmath-7.4.26-1.el8.remi.x86_64.rpm                                                410 kB/s |  88 kB     00:00
(14/31): php74-php-7.4.26-1.el8.remi.x86_64.rpm                                                       1.3 MB/s | 1.5 MB     00:01
(15/31): php74-php-common-7.4.26-1.el8.remi.x86_64.rpm                                                1.2 MB/s | 710 kB     00:00
(16/31): php74-php-cli-7.4.26-1.el8.remi.x86_64.rpm                                                   1.4 MB/s | 3.1 MB     00:02
(17/31): php74-php-gd-7.4.26-1.el8.remi.x86_64.rpm                                                    492 kB/s |  93 kB     00:00
(18/31): php74-php-fpm-7.4.26-1.el8.remi.x86_64.rpm                                                   1.8 MB/s | 1.6 MB     00:00
(19/31): php74-php-gmp-7.4.26-1.el8.remi.x86_64.rpm                                                   549 kB/s |  84 kB     00:00
(20/31): php74-php-intl-7.4.26-1.el8.remi.x86_64.rpm                                                  1.1 MB/s | 201 kB     00:00
(21/31): php74-php-json-7.4.26-1.el8.remi.x86_64.rpm                                                  610 kB/s |  82 kB     00:00
(22/31): php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64.rpm                                               844 kB/s | 200 kB     00:00
(23/31): php74-php-mbstring-7.4.26-1.el8.remi.x86_64.rpm                                              1.3 MB/s | 492 kB     00:00
(24/31): php74-php-opcache-7.4.26-1.el8.remi.x86_64.rpm                                               1.4 MB/s | 275 kB     00:00
(25/31): php74-php-pdo-7.4.26-1.el8.remi.x86_64.rpm                                                   635 kB/s | 130 kB     00:00
(26/31): php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64.rpm                                              359 kB/s |  58 kB     00:00
(27/31): php74-php-process-7.4.26-1.el8.remi.x86_64.rpm                                               629 kB/s |  92 kB     00:00
(28/31): php74-php-sodium-7.4.26-1.el8.remi.x86_64.rpm                                                426 kB/s |  87 kB     00:00
(29/31): php74-php-xml-7.4.26-1.el8.remi.x86_64.rpm                                                   991 kB/s | 180 kB     00:00
(30/31): libicu69-69.1-1.el8.remi.x86_64.rpm                                                          2.1 MB/s | 9.6 MB     00:04
(31/31): php74-runtime-1.0-3.el8.remi.x86_64.rpm                                                      3.2 MB/s | 1.1 MB     00:00
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 3.7 MB/s |  25 MB     00:06
Extra Packages for Enterprise Linux 8 - x86_64                                                        341 kB/s | 1.6 kB     00:00
Importing GPG key 0x2F86D6A1:
 Userid     : "Fedora EPEL (8) <epel@fedoraproject.org>"
 Fingerprint: 94E2 79EB 8D8F 25B2 1810 ADF1 21EA 45AB 2F86 D6A1
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
Is this ok [y/N]: y
Key imported successfully
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                                                                     1/31
  Installing       : libicu69-69.1-1.el8.remi.x86_64                                                                             2/31
  Installing       : libsodium-1.0.18-2.el8.x86_64                                                                               3/31
  Installing       : tcl-1:8.6.8-2.el8.x86_64                                                                                    4/31
  Running scriptlet: tcl-1:8.6.8-2.el8.x86_64                                                                                    4/31
  Installing       : environment-modules-4.5.2-1.el8.x86_64                                                                      5/31
  Running scriptlet: environment-modules-4.5.2-1.el8.x86_64                                                                      5/31
  Installing       : scl-utils-1:2.0.2-14.el8.x86_64                                                                             6/31
  Installing       : python3-setools-4.3.0-2.el8.x86_64                                                                          7/31
  Installing       : python3-libsemanage-2.9-6.el8.x86_64                                                                        8/31
  Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                      9/31
  Installing       : checkpolicy-2.9-1.el8.x86_64                                                                               10/31
  Installing       : python3-policycoreutils-2.9-16.el8.noarch                                                                  11/31
  Installing       : policycoreutils-python-utils-2.9-16.el8.noarch                                                             12/31
  Installing       : php74-runtime-1.0-3.el8.remi.x86_64                                                                        13/31
  Running scriptlet: php74-runtime-1.0-3.el8.remi.x86_64                                                                        13/31
  Installing       : php74-php-json-7.4.26-1.el8.remi.x86_64                                                                    14/31
  Installing       : php74-php-common-7.4.26-1.el8.remi.x86_64                                                                  15/31
  Installing       : php74-php-pdo-7.4.26-1.el8.remi.x86_64                                                                     16/31
  Installing       : php74-php-cli-7.4.26-1.el8.remi.x86_64                                                                     17/31
  Installing       : php74-php-fpm-7.4.26-1.el8.remi.x86_64                                                                     18/31
  Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                                                                     18/31
  Installing       : php74-php-mbstring-7.4.26-1.el8.remi.x86_64                                                                19/31
  Installing       : php74-php-opcache-7.4.26-1.el8.remi.x86_64                                                                 20/31
  Installing       : php74-php-sodium-7.4.26-1.el8.remi.x86_64                                                                  21/31
  Installing       : php74-php-xml-7.4.26-1.el8.remi.x86_64                                                                     22/31
  Installing       : php74-libzip-1.8.0-1.el8.remi.x86_64                                                                       23/31
  Installing       : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                                                                24/31
  Installing       : php74-php-7.4.26-1.el8.remi.x86_64                                                                         25/31
  Installing       : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64                                                                 26/31
  Installing       : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                                                                  27/31
  Installing       : php74-php-gd-7.4.26-1.el8.remi.x86_64                                                                      28/31
  Installing       : php74-php-gmp-7.4.26-1.el8.remi.x86_64                                                                     29/31
  Installing       : php74-php-intl-7.4.26-1.el8.remi.x86_64                                                                    30/31
  Installing       : php74-php-process-7.4.26-1.el8.remi.x86_64                                                                 31/31
  Running scriptlet: php74-php-process-7.4.26-1.el8.remi.x86_64                                                                 31/31
  Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                                                                     31/31
  Verifying        : scl-utils-1:2.0.2-14.el8.x86_64                                                                             1/31
  Verifying        : checkpolicy-2.9-1.el8.x86_64                                                                                2/31
  Verifying        : environment-modules-4.5.2-1.el8.x86_64                                                                      3/31
  Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch                                                              4/31
  Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                      5/31
  Verifying        : python3-libsemanage-2.9-6.el8.x86_64                                                                        6/31
  Verifying        : python3-policycoreutils-2.9-16.el8.noarch                                                                   7/31
  Verifying        : python3-setools-4.3.0-2.el8.x86_64                                                                          8/31
  Verifying        : tcl-1:8.6.8-2.el8.x86_64                                                                                    9/31
  Verifying        : libsodium-1.0.18-2.el8.x86_64                                                                              10/31
  Verifying        : libicu69-69.1-1.el8.remi.x86_64                                                                            11/31
  Verifying        : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                                                                    12/31
  Verifying        : php74-libzip-1.8.0-1.el8.remi.x86_64                                                                       13/31
  Verifying        : php74-php-7.4.26-1.el8.remi.x86_64                                                                         14/31
  Verifying        : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                                                                  15/31
  Verifying        : php74-php-cli-7.4.26-1.el8.remi.x86_64                                                                     16/31
  Verifying        : php74-php-common-7.4.26-1.el8.remi.x86_64                                                                  17/31
  Verifying        : php74-php-fpm-7.4.26-1.el8.remi.x86_64                                                                     18/31
  Verifying        : php74-php-gd-7.4.26-1.el8.remi.x86_64                                                                      19/31
  Verifying        : php74-php-gmp-7.4.26-1.el8.remi.x86_64                                                                     20/31
  Verifying        : php74-php-intl-7.4.26-1.el8.remi.x86_64                                                                    21/31
  Verifying        : php74-php-json-7.4.26-1.el8.remi.x86_64                                                                    22/31
  Verifying        : php74-php-mbstring-7.4.26-1.el8.remi.x86_64                                                                23/31
  Verifying        : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64                                                                 24/31
  Verifying        : php74-php-opcache-7.4.26-1.el8.remi.x86_64                                                                 25/31
  Verifying        : php74-php-pdo-7.4.26-1.el8.remi.x86_64                                                                     26/31
  Verifying        : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                                                                27/31
  Verifying        : php74-php-process-7.4.26-1.el8.remi.x86_64                                                                 28/31
  Verifying        : php74-php-sodium-7.4.26-1.el8.remi.x86_64                                                                  29/31
  Verifying        : php74-php-xml-7.4.26-1.el8.remi.x86_64                                                                     30/31
  Verifying        : php74-runtime-1.0-3.el8.remi.x86_64                                                                        31/31

Installed:
  checkpolicy-2.9-1.el8.x86_64                                  environment-modules-4.5.2-1.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                               libsodium-1.0.18-2.el8.x86_64
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64                       php74-libzip-1.8.0-1.el8.remi.x86_64
  php74-php-7.4.26-1.el8.remi.x86_64                            php74-php-bcmath-7.4.26-1.el8.remi.x86_64
  php74-php-cli-7.4.26-1.el8.remi.x86_64                        php74-php-common-7.4.26-1.el8.remi.x86_64
  php74-php-fpm-7.4.26-1.el8.remi.x86_64                        php74-php-gd-7.4.26-1.el8.remi.x86_64
  php74-php-gmp-7.4.26-1.el8.remi.x86_64                        php74-php-intl-7.4.26-1.el8.remi.x86_64
  php74-php-json-7.4.26-1.el8.remi.x86_64                       php74-php-mbstring-7.4.26-1.el8.remi.x86_64
  php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64                    php74-php-opcache-7.4.26-1.el8.remi.x86_64
  php74-php-pdo-7.4.26-1.el8.remi.x86_64                        php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64
  php74-php-process-7.4.26-1.el8.remi.x86_64                    php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64                        php74-runtime-1.0-3.el8.remi.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch                python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64                          python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64                            scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!

```

### Conf Apache

IncludeOptional conf.d/*.conf

```
[bagarre@web conf.d]$ sudo vi nextcloud.conf
[bagarre@web conf.d]$ sudo cat nextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[bagarre@web conf.d]$ sudo systemctl restart httpd
[bagarre@web conf.d]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Tue 2021-12-07 17:10:39 CET; 7s ago
     Docs: man:httpd.service(8)
 Main PID: 15243 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4943)
   Memory: 24.6M
   CGroup: /system.slice/httpd.service
           ├─15243 /usr/sbin/httpd -DFOREGROUND
           ├─15244 /usr/sbin/httpd -DFOREGROUND
           ├─15245 /usr/sbin/httpd -DFOREGROUND
           ├─15246 /usr/sbin/httpd -DFOREGROUND
           └─15247 /usr/sbin/httpd -DFOREGROUND

Dec 07 17:10:39 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 17:10:39 web.tp2.cesi httpd[15243]: AH00112: Warning: DocumentRoot [/var/www/nextcloud/html/] does not exist
Dec 07 17:10:39 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 17:10:39 web.tp2.cesi httpd[15243]: Server configured, listening on: port 80

```
#### Configure racine web

```
402  sudo mkdir /var/www/nextcloud
403  sudo mkdir /var/www/nextcloud/html
413  sudo chown apache /var/www/nextcloud
414  sudo chown apache /var/www/nextcloud/html
[bagarre@web html]$ ls -all
total 0
drwxr-xr-x. 2 apache root  6 Dec  7 17:14 .
drwxr-xr-x. 3 apache root 18 Dec  7 17:14 ..
```
#### Configurer PHP
```
[bagarre@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
[bagarre@web ~]$ timedatectl
               Local time: Tue 2021-12-07 17:34:15 CET
           Universal time: Tue 2021-12-07 16:34:15 UTC
                 RTC time: Tue 2021-12-07 16:29:40
                Time zone: Europe/Paris (CET, +0100)
```
### NextCloud

```
 [bagarre@web ~]$ sudo curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  2415k      0  0:01:02  0:01:02 --:--:-- 2796k

sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/

[bagarre@web nextcloud]$ sudo chown -R apache /var/www/nextcloud/html/
#### Test connexion 
````
[bagarre@web html]$ [bagarre@web html]$ curl http://web.tp2.cesi
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="3ciMsZ6Jm9pT/9fMqKQkyLDIRvzAx5nuYtBM+fADEjY=:7I3l4fDk3rcZheWOz/cSpdOAPo62osmmEr4ujpdRZ30=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
                <link rel="icon" href="/core/img/favicon.ico">
                <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
                <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
                <link rel="manifest" href="/core/img/manifest.json">
                <link rel="stylesheet" href="/core/css/guest.css?v=ba222ded25d957b900c03bef914333cd">
                <script nonce="M2NpTXNaNkptOXBULzlmTXFLUWt5TERJUnZ6QXg1bnVZdEJNK2ZBREVqWT06N0kzbDRmRGszcmNaaGVXT3ovY1NwZE9BUG82Mm9zbW1FcjR1anBkUlozMD0=" defer src="/core/js/dist/main.js?v=ba222ded25d957b900c03bef914333cd"></script>
<script nonce="M2NpTXNaNkptOXBULzlmTXFLUWt5TERJUnZ6QXg1bnVZdEJNK2ZBREVqWT06N0kzbDRmRGszcmNaaGVXT3ovY1NwZE9BUG82Mm9zbW1FcjR1anBkUlozMD0=" defer src="/core/js/dist/install.js?v=ba222ded25d957b900c03bef914333cd"></script>
                        </head>
        <body id="body-login">
                <noscript>
        <div id="nojavascript">
                <div>
                        This application requires JavaScript for correct operation. Please <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer noopener">enable JavaScript</a> and reload the page.                </div>
        </div>
</noscript>
                                <div class="wrapper">
                        <div class="v-align">
                                                                        <header role="banner">
                                                <div id="header">
                                                        <div class="logo">
                                                                <h1 class="hidden-visually">
                                                                        Nextcloud
        </h1>
                                                                                                                       </div>
                                                </div>
                                        </header>
                                                                <main>
                                        <input type='hidden' id='hasMySQL' value='1'>
<input type='hidden' id='hasSQLite' value='1'>
<input type='hidden' id='hasPostgreSQL' value=''>
<input type='hidden' id='hasOracle' value=''>
<form action="index.php" method="post">
<input type="hidden" name="install" value="true">
                        <fieldset id="adminaccount">
                <legend>Create an <strong>admin account</strong></legend>
                <p class="grouptop">
                        <input type="text" name="adminlogin" id="adminlogin"
                                placeholder="Username"
                                value=""
                                autocomplete="off" autocapitalize="none" autocorrect="off" autofocus required>
                        <label for="adminlogin" class="infield">Username</label>
                </p>
                <p class="groupbottom">
                        <input type="password" name="adminpass" data-typetoggle="#show" id="adminpass"
                                placeholder="Password"
                                value=""
                                autocomplete="off" autocapitalize="none" autocorrect="off" required>
                        <label for="adminpass" class="infield">Password</label>
                        <input type="checkbox" id="show" class="hidden-visually" name="show" aria-label="Show password">
                        <label for="show"></label>
                </p>
        </fieldset>

                <fieldset id="advancedHeader">
                <legend><a id="showAdvanced" tabindex="0" href="#">Storage &amp; database<img src="/core/img/actions/caret-white.svg" /></a></legend>
        </fieldset>

                <fieldset id="datadirField">
                <div id="datadirContent">
                        <label for="directory">Data folder</label>
                        <input type="text" name="directory" id="directory"
                                placeholder="/var/www/nextcloud/html/data"
                                value="/var/www/nextcloud/html/data"
                                autocomplete="off" autocapitalize="none" autocorrect="off">
                </div>
        </fieldset>

                <fieldset id='databaseBackend'>
                                <legend>Configure the database</legend>
                <div id="selectDbType">
                                                <input type="radio" name="dbtype" value="sqlite" id="sqlite"
                        />
                <label class="sqlite" for="sqlite">SQLite</label>
                                                                <input type="radio" name="dbtype" value="mysql" id="mysql"
                        />
                <label class="mysql" for="mysql">MySQL/MariaDB</label>
                                                </div>
        </fieldset>

                                <fieldset id='databaseField'>
                <div id="use_other_db">
                        <p class="grouptop">
                                <label for="dbuser" class="infield">Database user</label>
                                <input type="text" name="dbuser" id="dbuser"
                                        placeholder="Database user"
                                        value=""
                                        autocomplete="off" autocapitalize="none" autocorrect="off">
                        </p>
                        <p class="groupmiddle">
                                <input type="password" name="dbpass" id="dbpass" data-typetoggle="#dbpassword-toggle"
                                        placeholder="Database password"
                                        value=""
                                        autocomplete="off" autocapitalize="none" autocorrect="off">
                                <label for="dbpass" class="infield">Database password</label>
                                <input type="checkbox" id="dbpassword-toggle" class="hidden-visually" name="dbpassword-toggle" aria-label="Show password">
                                <label for="dbpassword-toggle"></label>
                        </p>
                        <p class="groupmiddle">
                                <label for="dbname" class="infield">Database name</label>
                                <input type="text" name="dbname" id="dbname"
                                        placeholder="Database name"
                                        value=""
                                        autocomplete="off" autocapitalize="none" autocorrect="off"
                                        pattern="[0-9a-zA-Z$_-]+">
                        </p>
                                                <p class="groupbottom">
                                <label for="dbhost" class="infield">Database host</label>
                                <input type="text" name="dbhost" id="dbhost"
                                        placeholder="Database host"
                                        value="localhost"
                                        autocomplete="off" autocapitalize="none" autocorrect="off">
                        </p>
                        <p class="info">
                                Please specify the port number along with the host name (e.g., localhost:5432).                            </p>
                </div>
                </fieldset>

                        <fieldset id="sqliteInformation" class="warning">
                        <legend>Performance warning</legend>
                        <p>You chose SQLite as database.</p>
                        <p>SQLite should only be used for minimal and development instances. For production we recommend a different database backend.</p>
                        <p>If you use clients for file syncing, the use of SQLite is highly discouraged.</p>
                </fieldset>

        <fieldset>
                <p class="info">
                        <input type="checkbox" id="install-recommended-apps" name="install-recommended-apps" class="checkbox checkbox--white" checked>
                        <label for="install-recommended-apps">
                                Install recommended apps                                <span>Calendar, Contacts, Talk, Mail &amp; Collaborative editing</span>
                        </label>
                </p>
        </fieldset>

        <div class="icon-loading-dark float-spinner">&nbsp;</div>

        <div class="buttons"><input type="submit" class="primary" value="Finish setup" data-finishing="Finishing …"></div>

        <p class="info">
                <span class="icon-info-white"></span>
                Need help?              <a target="_blank" rel="noreferrer noopener" href="https://docs.nextcloud.com/server/21/go.php?to=admin-install">See the documentation ↗</a>
        </p>
</form>
                                </main>
                        </div>
                </div>
                <footer role="contentinfo">
                        <p class="info">
                                <a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener">Nextcloud</a> – a safe home for all your data                     </p>
                </footer>
        </body>
</html>
```

# FIN PARTIE 1
---
# PARTIE 2

### Fail2Ban
```
[bagarre@web ~]$ sudo systemctl start firewalld
[bagarre@web ~]$ sudo systemctl enable firewalld
[bagarre@web ~]$ sudo dnf install epel-release -y
Last metadata expiration check: 0:44:37 ago on Tue 07 Dec 2021 08:06:02 PM CET.
Package epel-release-8-13.el8.noarch is already installed.
[bagarre@web ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
[sudo] password for bagarre:
Last metadata expiration check: 17:28:03 ago on Tue 07 Dec 2021 08:06:02 PM CET.
Dependencies resolved.
============================================================================================================================================
 Package                                 Architecture                Version                           Repository                      Size
============================================================================================================================================
Installing:
 fail2ban                                noarch                      0.11.2-1.el8                      epel                            19 k
 fail2ban-firewalld                      noarch                      0.11.2-1.el8                      epel                            19 k
Installing dependencies:
 esmtp                                   x86_64                      1.2-15.el8                        epel                            57 k
 fail2ban-sendmail                       noarch                      0.11.2-1.el8                      epel                            22 k
 fail2ban-server                         noarch                      0.11.2-1.el8                      epel                           459 k
 libesmtp                                x86_64                      1.0.6-18.el8                      epel                            70 k
 liblockfile                             x86_64                      1.14-1.el8                        appstream                       31 k
 python3-systemd                         x86_64                      234-8.el8                         appstream                       80 k

Transaction Summary
============================================================================================================================================
Install  8 Packages

Total download size: 756 k
Installed size: 1.9 M
Downloading Packages:
(1/8): liblockfile-1.14-1.el8.x86_64.rpm                                                                    294 kB/s |  31 kB     00:00
(2/8): esmtp-1.2-15.el8.x86_64.rpm                                                                          445 kB/s |  57 kB     00:00
(3/8): python3-systemd-234-8.el8.x86_64.rpm                                                                 610 kB/s |  80 kB     00:00
(4/8): fail2ban-0.11.2-1.el8.noarch.rpm                                                                     708 kB/s |  19 kB     00:00
(5/8): fail2ban-firewalld-0.11.2-1.el8.noarch.rpm                                                           828 kB/s |  19 kB     00:00
(6/8): fail2ban-sendmail-0.11.2-1.el8.noarch.rpm                                                            840 kB/s |  22 kB     00:00
(7/8): libesmtp-1.0.6-18.el8.x86_64.rpm                                                                     1.2 MB/s |  70 kB     00:00
(8/8): fail2ban-server-0.11.2-1.el8.noarch.rpm                                                              1.6 MB/s | 459 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                       525 kB/s | 756 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                    1/1
  Installing       : libesmtp-1.0.6-18.el8.x86_64                                                                                       1/8
  Installing       : python3-systemd-234-8.el8.x86_64                                                                                   2/8
  Installing       : fail2ban-server-0.11.2-1.el8.noarch                                                                                3/8
  Running scriptlet: fail2ban-server-0.11.2-1.el8.noarch                                                                                3/8
  Installing       : fail2ban-firewalld-0.11.2-1.el8.noarch                                                                             4/8
  Installing       : liblockfile-1.14-1.el8.x86_64                                                                                      5/8
  Running scriptlet: liblockfile-1.14-1.el8.x86_64                                                                                      5/8
  Installing       : esmtp-1.2-15.el8.x86_64                                                                                            6/8
  Running scriptlet: esmtp-1.2-15.el8.x86_64                                                                                            6/8
  Installing       : fail2ban-sendmail-0.11.2-1.el8.noarch                                                                              7/8
  Installing       : fail2ban-0.11.2-1.el8.noarch                                                                                       8/8
  Running scriptlet: fail2ban-0.11.2-1.el8.noarch                                                                                       8/8
  Verifying        : liblockfile-1.14-1.el8.x86_64                                                                                      1/8
  Verifying        : python3-systemd-234-8.el8.x86_64                                                                                   2/8
  Verifying        : esmtp-1.2-15.el8.x86_64                                                                                            3/8
  Verifying        : fail2ban-0.11.2-1.el8.noarch                                                                                       4/8
  Verifying        : fail2ban-firewalld-0.11.2-1.el8.noarch                                                                             5/8
  Verifying        : fail2ban-sendmail-0.11.2-1.el8.noarch                                                                              6/8
  Verifying        : fail2ban-server-0.11.2-1.el8.noarch                                                                                7/8
  Verifying        : libesmtp-1.0.6-18.el8.x86_64                                                                                       8/8

Installed:
  esmtp-1.2-15.el8.x86_64                       fail2ban-0.11.2-1.el8.noarch                fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch         fail2ban-server-0.11.2-1.el8.noarch         libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64                 python3-systemd-234-8.el8.x86_64

Complete!
[bagarre@web ~]$ sudo systemctl start fail2ban
[bagarre@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.

[bagarre@web ~]$ sudo vi /etc/fail2ban/jail.local
sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
[bagarre@web ~]$ sudo systemctl restart fail2ban

[bagarre@web ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:43:33 CET; 5s ago
     Docs: man:fail2ban(1)
  Process: 6466 ExecStop=/usr/bin/fail2ban-client stop (code=exited, status=0/SUCCESS)
  Process: 6520 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 6521 (fail2ban-server)
    Tasks: 3 (limit: 4956)
   Memory: 11.0M
   CGroup: /system.slice/fail2ban.service
           └─6521 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start
```
#### SSH jail

```
[bagarre@web ~]$ sudo vi /etc/fail2ban/jail.d/sshd.local
[bagarre@web ~]$ sudo systemctl restart fail2ban
```
### NGINX
```
[bagarre@proxy ~]$ [bagasystemctl status nginx
[sudo] password for bagarre:
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 14:07:48 CET; 11min ago
  Process: 1677 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1675 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1674 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1679 (nginx)
    Tasks: 2 (limit: 4943)
   Memory: 14.0M
   CGroup: /system.slice/nginx.service
           ├─1679 nginx: master process /usr/sbin/nginx
           └─1680 nginx: worker process

Dec 08 14:07:48 proxy.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 08 14:07:48 proxy.tp2.cesi nginx[1675]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 08 14:07:48 proxy.tp2.cesi nginx[1675]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 08 14:07:48 proxy.tp2.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 08 14:07:48 proxy.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
```



## Part 3

### Netdata
```
[bagarre@web ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-KgBU5NLJGc]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-KgBU5NLJGc/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[/tmp/netdata-kickstart-KgBU5NLJGc]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-KgBU5NLJGc/netdata-latest.gz.run https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run
 OK

 --- Installing netdata ---
[/tmp/netdata-kickstart-KgBU5NLJGc]$ sudo sh /tmp/netdata-kickstart-KgBU5NLJGc/netdata-latest.gz.run -- --auto-update
[sudo] password for bagarre:

  ^
  |.-.   .-.   .-.   .-.   .  netdata
  |   '-'   '-'   '-'   '-'   real-time performance monitoring, done right!
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  (C) Copyright 2017, Costa Tsaousis
  All rights reserved
  Released under GPL v3+

  You are about to install netdata to this system.
  netdata will be installed at:

                    /opt/netdata

  The following changes will be made to your system:

  # USERS / GROUPS
  User 'netdata' and group 'netdata' will be added, if not present.

  # LOGROTATE
  This file will be installed if logrotate is present.

   - /etc/logrotate.d/netdata

  # SYSTEM INIT
  This file will be installed if this system runs with systemd:

   - /lib/systemd/system/netdata.service

   or, for older CentOS, Debian/Ubuntu or OpenRC Gentoo:

   - /etc/init.d/netdata         will be created


  This package can also update a netdata installation that has been
  created with another version of it.

  Your netdata configuration will be retained.
  After installation, netdata will be (re-)started.

  netdata re-distributes a lot of open source software components.
  Check its full license at:
  https://github.com/netdata/netdata/blob/master/LICENSE.md
Please type y to accept, n otherwise: y
Creating directory /opt/netdata
Verifying archive integrity...  100%   All good.
Uncompressing netdata, the real-time performance and health monitoring system  100%
 --- Deleting stock configuration files from user configuration directory ---
 --- Attempt to create user/group netdata/netadata ---
Adding netdata user group ...
[/opt/netdata]# groupadd -r netdata
 OK

Adding netdata user account with home /opt/netdata ...
[/opt/netdata]# useradd -r -g netdata -c netdata -s /sbin/nologin --no-create-home -d /opt/netdata netdata
 OK

 --- Add user netdata to required user groups ---
Group 'docker' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Adding netdata user to the nginx group ...
[/opt/netdata]# usermod -a -G nginx netdata
 OK

Group 'varnish' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'haproxy' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Adding netdata user to the adm group ...
[/opt/netdata]# usermod -a -G adm netdata
 OK

Group 'nsd' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'proxy' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'squid' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'ceph' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Adding netdata user to the nobody group ...
[/opt/netdata]# usermod -a -G nobody netdata
 OK

Group 'I2C' does not exist.
 FAILED  Failed to add netdata user to secondary groups

 --- Install logrotate configuration for netdata ---
[/opt/netdata]# cp system/netdata.logrotate /etc/logrotate.d/netdata
 OK

[/opt/netdata]# chmod 644 /etc/logrotate.d/netdata
 OK

 --- Telemetry configuration ---
You can opt out from anonymous statistics via the --disable-telemetry option, or by creating an empty file /opt/netdata/etc/netdata/.opt-out-from-anonymous-statistics

 --- Install netdata at system init ---
Installing systemd service...
[/opt/netdata]# cp system/netdata.service /lib/systemd/system/netdata.service
 OK

[/opt/netdata]# systemctl daemon-reload
 OK

[/opt/netdata]# systemctl enable netdata
Created symlink /etc/systemd/system/multi-user.target.wants/netdata.service -> /usr/lib/systemd/system/netdata.service.
 OK

 --- Install (but not enable) netdata updater tool ---
Failed to disable unit: Unit file netdata-updater.timer does not exist.
cat: /system/netdata-updater.timer: No such file or directory
cat: /system/netdata-updater.service: No such file or directory
Update script is located at /opt/netdata/usr/libexec/netdata/netdata-updater.sh

 --- Check if we must enable/disable the netdata updater tool ---
Auto-updating has been enabled through cron, updater script linked to /etc/cron.daily/netdata-updater

If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Successful updates will not send an email.

 --- creating quick links ---
[/opt/netdata]# ln -s bin sbin
 OK

[/opt/netdata/usr]# ln -s ../bin bin
 OK

[/opt/netdata/usr]# ln -s ../bin sbin
 OK

[/opt/netdata/usr]# ln -s . local
 OK

[/opt/netdata]# ln -s etc/netdata netdata-configs
 OK

[/opt/netdata]# ln -s usr/share/netdata/web netdata-web-files
 OK

[/opt/netdata]# ln -s usr/libexec/netdata netdata-plugins
 OK

[/opt/netdata]# ln -s var/lib/netdata netdata-dbs
 OK

[/opt/netdata]# ln -s var/cache/netdata netdata-metrics
 OK

[/opt/netdata]# ln -s var/log/netdata netdata-logs
 OK

[/opt/netdata/etc/netdata]# rm orig
 OK

[/opt/netdata/etc/netdata]# ln -s ../../usr/lib/netdata/conf.d orig
 OK

 --- fix permissions ---
[/opt/netdata]# chmod g+rx\,o+rx /opt
 OK

[/opt/netdata]# chown -R netdata:netdata /opt/netdata
 OK

 --- fix plugin permissions ---
[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/apps.plugin
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/apps.plugin
 OK

[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/ioping
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/ioping
 OK

[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/cgroup-network
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/cgroup-network
 OK

[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/ebpf.plugin
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/ebpf.plugin
 OK

[/opt/netdata]# chown root:netdata bin/fping
 OK

[/opt/netdata]# chmod 4750 bin/fping
 OK

Configure TLS certificate paths
Using /etc/pki/tls for TLS configuration and certificates
Save install options
 --- starting netdata ---
 --- Restarting netdata instance ---

Stopping all netdata threads
[/opt/netdata]# stop_all_netdata
 OK

Starting netdata using command 'systemctl start netdata'
[/opt/netdata]# systemctl start netdata
 OK

Downloading default configuration from netdata...
[/opt/netdata]# curl -sSL --connect-timeout 10 --retry 3 http://localhost:19999/netdata.conf
 OK

[/opt/netdata]# mv /opt/netdata/etc/netdata/netdata.conf.new /opt/netdata/etc/netdata/netdata.conf
 OK

 OK  New configuration saved for you to edit at /opt/netdata/etc/netdata/netdata.conf


  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

[/opt/netdata]# chmod 0644 /opt/netdata/etc/netdata/netdata.conf
 OK

 OK

[/tmp/netdata-kickstart-KgBU5NLJGc]$ sudo rm /tmp/netdata-kickstart-KgBU5NLJGc/netdata-latest.gz.run
 OK

[/tmp/netdata-kickstart-KgBU5NLJGc]$ sudo rm -rf /tmp/netdata-kickstart-KgBU5NLJGc
 OK

[bagarre@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[bagarre@web ~]$ sudo firewall-cmd --reload
success
[bagarre@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp 80/tcp 443/tcp 1230/tcp 8888/tcp 3306/tcp 19999/tcp
 ```
```
[bagarre@web ~]$ curl web.tp2.cesi:19999
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" content="netdata"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width,initial-scale=1"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><meta name="author" content="costa@tsaousis.gr"><link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAP9JREFUeNpiYBgFo+A/w34gpiZ8DzWzAYgNiHGAA5UdgA73g+2gcyhgg/0DGQoweB6IBQYyFCCOGOBQwBMd/xnW09ERDtgcoEBHB+zHFQrz6egIBUasocDAcJ9OxWAhE4YQI8MDILmATg7wZ8QRDfQKhQf4Cie6pAVGPA4AhQKo0BCgZRAw4ZSBpIWJNI6CD4wEKikBaFqgVSgcYMIrzcjwgcahcIGRiPYCLUPBkNhWUwP9akVcoQBpatG4MsLviAIqWj6f3Absfdq2igg7IIEKDVQKEzN5ofAenJCp1I8gJRTug5tfkGIdR1FDniMI+QZUjF8Amn5htOdHCAAEGACE6B0cS6mrEwAAAABJRU5ErkJggg=="/><meta property="og:locale" content="en_US"/><meta property="og:url" content="https://my-netdata.io"/><meta property="og:type" content="website"/><meta property="og:site_name" content="netdata"/><meta property="og:title" content="Get control of your Linux Servers. Simple. Effective. Awesome."/><meta property="og:description" content="Unparalleled insights, in real-time, of everything happening on your Linux systems and applications, with stunning, interactive web dashboards and powerful performance and health alarms."/><meta property="og:image" content="https://cloud.githubusercontent.com/assets/2662304/22945737/e98cd0c6-f2fd-11e6-96f1-5501934b0955.png"/><meta property="og:image:type" content="image/png"/><meta property="fb:app_id" content="1200089276712916"/><meta name="twitter:card" content="summary"/><meta name="twitter:site" content="@linuxnetdata"/><meta name="twitter:title" content="Get control of your Linux Servers. Simple. Effective. Awesome."/><meta name="twitter:description" content="Unparalleled insights, in real-time, of everything happening on your Linux systems and applications, with stunning, interactive web dashboards and powerful performance and health alarms."/><meta name="twitter:image" content="https://cloud.githubusercontent.com/assets/2662304/14092712/93b039ea-f551-11e5-822c-beadbf2b2a2e.gif"/><style>.loadOverlay{position:fixed;top:0;left:0;width:100%;height:100%;z-index:2000;font-size:10vh;font-family:sans-serif;padding:40vh 0 40vh 0;font-weight:700;text-align:center}</style><link href="./static/css/2.20fd0a40.chunk.css" rel="stylesheet"><link href="./static/css/main.a46a34fa.chunk.css" rel="stylesheet"></head><body data-spy="scroll" data-target="#sidebar" data-offset="100"><div id="loadOverlay" class="loadOverlay" style="background-color:#fff;color:#888"><div style="font-size:3vh">You must enable JavaScript in order to use Netdata!<br/>You can do this in <a href="https://enable-javascript.com/" target="_blank">your browser settings</a>.</div></div><script type="text/javascript">// Cleanup JS warning.
        document.documentElement.style.overflowY = "scroll";
   ```